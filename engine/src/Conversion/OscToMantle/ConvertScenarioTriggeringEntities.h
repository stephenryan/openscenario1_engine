#pragma once

#include <openScenarioLib/generated/v1_1/api/ApiClassInterfacesV1_1.h>

#include <memory>
#include <string>

namespace OPENSCENARIO
{
enum TriggeringEntitiesRuleEnum
{
  UNKNOWN = -1,
  ALL,
  ANY,
};

struct TriggeringEntities
{
  //TODO: Need to implement the correct types
  TriggeringEntitiesRule triggeringEntitiesRule;
  std::vector<std::shared_ptr<IEntityRef>> entityRefs;
  int entityRefsSize;
};

inline TriggeringEntities ConvertScenarioTriggeringEntities(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITriggeringEntities> triggeringEntities)
{
  return {
      triggeringEntities->GetTriggeringEntitiesRule(),
      triggeringEntities->GetEntityRefs(),
      triggeringEntities->GetEntityRefsSize()

  };
}

}  // namespace OPENSCENARIO
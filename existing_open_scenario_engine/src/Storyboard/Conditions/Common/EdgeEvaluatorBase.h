/*******************************************************************************
 * Copyright (c) 2021 in-tech GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "EdgeEvaluatorBase.h"

namespace OPENSCENARIO
{

class EdgeEvaluatorBase
{
  public:
    virtual ~EdgeEvaluatorBase() = default;
    virtual void UpdateStatus(bool is_satisfied) = 0;

    bool GetStatus() const noexcept { return status_; };

  protected:
    bool status_{false};
};

}  // namespace OPENSCENARIO

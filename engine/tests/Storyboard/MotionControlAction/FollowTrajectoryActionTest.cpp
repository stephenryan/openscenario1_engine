/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Test/test_utils.h>
#include <gtest/gtest.h>

#include "Conversion/OscToMantle/ConvertScenarioTimeReference.h"
#include "Conversion/OscToMantle/ConvertScenarioTrajectoryFollowingMode.h"
#include "Conversion/OscToMantle/ConvertScenarioTrajectoryRef.h"
#include "Storyboard/MotionControlAction/FollowTrajectoryAction_impl.h"
#include "gmock/gmock.h"

using namespace mantle_api;
using testing::_;
using testing::Return;
using testing::SaveArg;
using namespace units::literals;

TEST(FollowTrajectoryAction, GivenMatchingControlStrategyGoalIsReached_ReturnsTrue)
{
  auto mockEnvironment = std::make_shared<MockEnvironment>();
  ON_CALL(*mockEnvironment,
          HasControlStrategyGoalBeenReached(_, ControlStrategyType::kFollowTrajectory))
      .WillByDefault(Return(true));

  OpenScenarioEngine::v1_1::FollowTrajectoryAction followTrajectoryAction({std::vector<std::string>{"Vehicle1"},
                                                                           0.0,
                                                                           Trajectory{},
                                                                           OPENSCENARIO::TimeReference{},
                                                                           OPENSCENARIO::TrajectoryFollowingMode{},
                                                                           OPENSCENARIO::TrajectoryRef{}},
                                                                          {mockEnvironment});

  ASSERT_TRUE(followTrajectoryAction.HasControlStrategyGoalBeenReached("Vehicle1"));
}

TEST(FollowTrajectoryAction, GivenUnmatchingControlStrategyGoalIsReached_ReturnsFalse)
{
  auto mockEnvironment = std::make_shared<MockEnvironment>();
  ON_CALL(*mockEnvironment,
          HasControlStrategyGoalBeenReached(_, ControlStrategyType::kUndefined))
      .WillByDefault(Return(false));

  OpenScenarioEngine::v1_1::FollowTrajectoryAction followTrajectoryAction({std::vector<std::string>{"Vehicle1"},
                                                                           0.0,
                                                                           Trajectory{},
                                                                           OPENSCENARIO::TimeReference{},
                                                                           OPENSCENARIO::TrajectoryFollowingMode{},
                                                                           OPENSCENARIO::TrajectoryRef{}},
                                                                          {mockEnvironment});

  ASSERT_FALSE(followTrajectoryAction.HasControlStrategyGoalBeenReached("Vehicle1"));
}

TEST(FollowTrajectoryAction, GivenFollowTrajectoryAction_WhenNoTrajectoryRef_ThenThrowError)
{
  auto mockEnvironment = std::make_shared<MockEnvironment>();

  OpenScenarioEngine::v1_1::FollowTrajectoryAction followTrajectoryAction({std::vector<std::string>{"Vehicle1"},
                                                                           0.0,
                                                                           Trajectory{},
                                                                           OPENSCENARIO::TimeReference{},
                                                                           OPENSCENARIO::TrajectoryFollowingMode{},
                                                                           OPENSCENARIO::TrajectoryRef{}},
                                                                          {mockEnvironment});

  EXPECT_THROW(followTrajectoryAction.SetControlStrategy(), std::runtime_error);
}

TEST(FollowTrajectoryAction, GivenFollowTrajectoryActionAndTrajectoryRef_UpdatesControlStrategies)
{
  Pose pose1{{1.0_m, 2.0_m, 0.0_m}, {0.1_rad, 0.0_rad, 0.0_rad}};
  Pose pose2{{1.1_m, 2.1_m, 0.0_m}, {0.2_rad, 0.0_rad, 0.0_rad}};
  Pose pose3{{1.2_m, 2.2_m, 0.0_m}, {0.3_rad, 0.0_rad, 0.0_rad}};

  PolyLine polyLine{
      {pose1, std::nullopt},
      {pose2, std::nullopt},
      {pose3, std::nullopt}};

  Trajectory setTrajectory{};
  setTrajectory.type = polyLine;

  auto mockEnvironment = std::make_shared<MockEnvironment>();
  auto mockGeometryHelper = static_cast<const MockGeometryHelper*>(mockEnvironment->GetGeometryHelper());

  ON_CALL(*mockGeometryHelper, TranslateGlobalPositionLocally(pose1.position, _, _)).WillByDefault(Return(pose1.position));
  ON_CALL(*mockGeometryHelper, TranslateGlobalPositionLocally(pose2.position, _, _)).WillByDefault(Return(pose2.position));
  ON_CALL(*mockGeometryHelper, TranslateGlobalPositionLocally(pose3.position, _, _)).WillByDefault(Return(pose3.position));

  std::vector<std::shared_ptr<ControlStrategy>> control_strategies{};
  EXPECT_CALL(*mockEnvironment, UpdateControlStrategies(_, _)).WillOnce(SaveArg<1>(&control_strategies));

  OpenScenarioEngine::v1_1::FollowTrajectoryAction followTrajectoryAction({std::vector<std::string>{"Vehicle1"},
                                                                           0.0,
                                                                           Trajectory{},
                                                                           OPENSCENARIO::TimeReference{},
                                                                           OPENSCENARIO::TrajectoryFollowingMode{},
                                                                           setTrajectory},
                                                                          {mockEnvironment});

  EXPECT_NO_THROW(followTrajectoryAction.SetControlStrategy());

  EXPECT_THAT(control_strategies, testing::SizeIs(1));
  EXPECT_EQ(control_strategies.front()->type, ControlStrategyType::kFollowTrajectory);
  auto& trajectory = std::dynamic_pointer_cast<FollowTrajectoryControlStrategy>(control_strategies.front())->trajectory;
  auto& polyline = std::get<PolyLine>(trajectory.type);
  EXPECT_THAT(polyline, testing::SizeIs(3));
  EXPECT_EQ(polyline.at(0).pose, pose1);
  EXPECT_EQ(polyline.at(1).pose, pose2);
  EXPECT_EQ(polyline.at(2).pose, pose3);
}
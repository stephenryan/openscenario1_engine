/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToNode/ParseAct.h"

#include <memory>

#include "Conversion/OscToNode/ParseManeuverGroups.h"
#include "Conversion/OscToNode/ParseTrigger.h"
#include "Node/ActNode.h"

yase::BehaviorNode::Ptr parse(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IAct> act)
{
  return std::make_shared<yase::ActNode>(
      "Act",
      parse(act->GetManeuverGroups()),
      parse(act->GetStartTrigger()),
      parse(act->GetStopTrigger()));
}

/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *                     in-tech GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>
#include <openScenarioLib/generated/v1_1/api/ApiClassInterfacesV1_1.h>

#include <memory>

namespace OPENSCENARIO
{
class TrafficSignalCondition
{
  public:
    TrafficSignalCondition(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITrafficSignalCondition> condition,
                           std::shared_ptr<mantle_api::IEnvironment> environment)
        : environment_{environment}
    /*, TODO: extract info from  condition */ { std::ignore = condition; }

    bool IsSatisfied() const;

  private:
    std::shared_ptr<mantle_api::IEnvironment> environment_;
};

}  // namespace OPENSCENARIO

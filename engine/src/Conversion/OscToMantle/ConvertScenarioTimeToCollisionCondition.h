#pragma once

#include <openScenarioLib/generated/v1_1/api/ApiClassInterfacesV1_1.h>

#include <memory>
#include <string>

#include "Conversion/OscToMantle/ConvertScenarioTimeToCollisionConditionTarget.h"

namespace OPENSCENARIO
{
struct TimeToCollisionCondition
{
  //TODO: Need to implement the correct types
  bool alongRoute;
  CoordinateSystem coordinateSystem;
  bool freeSpace;
  RelativeDistanceType relativeDistanceType;
  Rule rule;
  double value;
  TimeToCollisionConditionTarget timeToCollisionConditionTarget;
};

inline TimeToCollisionCondition ConvertScenarioTimeToCollisionCondition(
    std::shared_ptr<mantle_api::IEnvironment> environment,
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITimeToCollisionCondition> timeToCollisionCondition)
{
  return {
      timeToCollisionCondition->GetAlongRoute(),
      timeToCollisionCondition->GetCoordinateSystem(),
      timeToCollisionCondition->GetFreespace(),
      timeToCollisionCondition->GetRelativeDistanceType(),
      timeToCollisionCondition->GetRule(),
      timeToCollisionCondition->GetValue(),
      ConvertScenarioTimeToCollisionConditionTarget(environment, timeToCollisionCondition->GetTimeToCollisionConditionTarget())

  };
}

}  // namespace OPENSCENARIO
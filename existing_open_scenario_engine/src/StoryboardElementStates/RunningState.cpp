/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RunningState.h"

namespace OPENSCENARIO
{

void RunningState::Step() {}

void RunningState::SetContext(IStateMachine& context)
{
    context_ = &context;
}
void RunningState::SetTransitionToState(IState& next_state)
{
    next_state_ = &next_state;
}

}  // namespace OPENSCENARIO

/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Storyboard/ByEntityCondition/RelativeDistanceCondition_impl.h"

#include "Utils/EntityUtils.h"

namespace OpenScenarioEngine::v1_1
{
using namespace OPENSCENARIO;

bool RelativeDistanceCondition::IsSatisfied() const
{
  if (values.relativeDistanceType == OPENSCENARIO::RelativeDistanceType::kLongitudinal &&
      values.coordinateSystem == OPENSCENARIO::CoordinateSystem::kEntity)
  {
    const auto& triggeringEntity = EntityUtils::GetEntityByName(mantle.environment, values.triggeringEntity);
    const auto& referencedEntity = EntityUtils::GetEntityByName(mantle.environment, values.entityRef);

    const auto distance = values.freespace
                              ? EntityUtils::CalculateLongitudinalFreeSpaceDistance(
                                    mantle.environment, triggeringEntity, referencedEntity)
                              : EntityUtils::CalculateRelativeLongitudinalDistance(
                                    mantle.environment, triggeringEntity, referencedEntity);

    return values.rule.IsSatisfied(distance.value());
  }

  std::cout << "RelativeDistanceCondition: Selected relativeDistanceType or coordinateSystem not implemented yet. Returning false;\n";
  return false;
}
}  // namespace OpenScenarioEngine::v1_1
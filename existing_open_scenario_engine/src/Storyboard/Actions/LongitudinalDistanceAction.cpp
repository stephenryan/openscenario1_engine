/*******************************************************************************
 * Copyright (c) 2022, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "LongitudinalDistanceAction.h"

#include "Storyboard/Utils/EntityUtils.h"

#include <MantleAPI/Traffic/entity_helper.h>

namespace OPENSCENARIO
{

LongitudinalDistanceAction::LongitudinalDistanceAction(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ILongitudinalDistanceAction> longitudinal_distance_action_data,
    std::shared_ptr<mantle_api::IEnvironment> environment,
    const std::vector<std::string>& actors)
    : MotionControlAction(environment, actors), longitudinal_distance_action_data_(longitudinal_distance_action_data)
{
}

bool LongitudinalDistanceAction::HasControlStrategyGoalBeenReached(const std::string& actor)
{
    auto entity = environment_->GetEntityRepository().Get(actor);
    if (!entity.has_value())
    {
        std::cout << "Warning: Entity with name \"" + actor +
                         "\" for LongitudinalDistanceAction does not exist. Please adjust the scenario.\n";
        return true;
    }
    return (!longitudinal_distance_action_data_->IsSetDynamicConstraints() &&
            !longitudinal_distance_action_data_->GetContinuous());
}

std::optional<mantle_api::Direction> LongitudinalDistanceAction::ResolveDirection(
    const NET_ASAM_OPENSCENARIO::v1_1::LongitudinalDisplacement& displacement)
{
    switch (displacement.GetFromLiteral(displacement.GetLiteral()))
    {

        case NET_ASAM_OPENSCENARIO::v1_1::LongitudinalDisplacement::LEADING_REFERENCED_ENTITY:
            return mantle_api::Direction::kForward;
        case NET_ASAM_OPENSCENARIO::v1_1::LongitudinalDisplacement::TRAILING_REFERENCED_ENTITY:
            return mantle_api::Direction::kBackwards;
        case NET_ASAM_OPENSCENARIO::v1_1::LongitudinalDisplacement::ANY:
            // TODO: resolve the direction from the relative position of actor and reference entity
            return {};
    }
    throw std::runtime_error(
        "LongitudinalDistanceAction: resolve the given LongitudinalDisplacement to direction is not supported.");
}

bool LongitudinalDistanceAction::InitActionProperties()
{

    if (longitudinal_distance_action_data_->GetContinuous())
    {
        std::cout << "Warning: LongitudinalDistanceAction:  \"continuous \" is not implemented.\n";
        return false;
    }

    if (longitudinal_distance_action_data_->IsSetDynamicConstraints())
    {
        std::cout << "Warning: LongitudinalDistanceAction:  \"dynamicConstraints\" is not implemented.\n";
        return false;
    }

    if (!(longitudinal_distance_action_data_->IsSetCoordinateSystem() &&
          longitudinal_distance_action_data_->GetCoordinateSystem() ==
              NET_ASAM_OPENSCENARIO::v1_1::CoordinateSystem::LANE))
    {
        std::cout << "Warning: LongitudinalDistanceAction: only \"CoordinateSystem::LANE\" is implemented.\n";
        return false;
    }

    const auto direction = ResolveDirection(longitudinal_distance_action_data_->GetDisplacement());
    if (direction.has_value() &&
        (direction == mantle_api::Direction::kForward || direction == mantle_api::Direction::kBackwards))
    {
        direction_ = direction.value();
    }
    else
    {
        std::cout << "Warning: LongitudinalDisplacement does not support ANY displacement.\n";
        return false;
    }

    if (longitudinal_distance_action_data_->IsSetDistance())
    {
        distance_ = longitudinal_distance_action_data_->GetDistance();
    }
    else if (longitudinal_distance_action_data_->IsSetTimeGap())
    {
        distance_ = longitudinal_distance_action_data_->GetTimeGap();
    }
    else
    {
        std::cout << "Warning: LongitudinalDistanceAction: please set either \"distance\" or \"timegap\". \n";
        return false;
    }

    if (distance_ < 0)
    {
        std::cout << "Warning: LongitudinalDistanceAction: \"distance\" or \"timegap\" can not be negative, Please "
                     "adjust the scenario. \n";
        return false;
    }

    return true;
}

void LongitudinalDistanceAction::StartAction()
{

    if (!InitActionProperties())
    {
        return;
    }

    for (const auto& actor : actors_)
    {
        const auto& ref_name_entity = longitudinal_distance_action_data_->GetEntityRef()->GetNameRef();
        const auto& ref_entity = EntityUtils::GetEntityByName(environment_, ref_name_entity);
        auto& actor_entity = EntityUtils::GetEntityByName(environment_, actor);

        auto requested_distance = longitudinal_distance_action_data_->IsSetTimeGap()
                                      ? distance_ * ref_entity.GetVelocity().Length().value()
                                      : distance_;

        if (longitudinal_distance_action_data_->GetFreespace())
        {
            std::cout << "Warning: LongitudinalDistanceAction: Please note that the \"freespace\" attribute is correct "
                         "only for a straight road in the lane coordinate system and when the orientation of both "
                         "entities is aligned with the lane centerline.\n";

            const auto ref_length = ref_entity.GetProperties()->bounding_box.dimension.length;
            const auto actor_length = actor_entity.GetProperties()->bounding_box.dimension.length;
            requested_distance += ref_length.value() / 2 + actor_length.value() / 2;
        }

        const mantle_api::Pose pose_ref{ref_entity.GetPosition(), ref_entity.GetOrientation()};
        if (const auto lane_pose = environment_->GetQueryService().FindLanePoseAtDistanceFrom(
                pose_ref, units::length::meter_t(requested_distance), direction_))
        {
            // longitudinal distance and lateral shift is calculated using entity ref as object reference.
            // actor pose is currently discarded in the calculations.
            const auto vertical_offset = actor_entity.GetProperties()->bounding_box.dimension.height / 2.0;
            const auto position_on_road = environment_->GetQueryService().GetUpwardsShiftedLanePosition(
                lane_pose.value().position, vertical_offset.value(), true);
            actor_entity.SetPosition(position_on_road);
            actor_entity.SetOrientation(lane_pose.value().orientation);
            auto ref_entity_speed = ref_entity.GetVelocity().Length();
            mantle_api::SetSpeed(&actor_entity, ref_entity_speed);
        }
        else
        {
            std::cout << "Warning: The pose for the entity with name \"" + actor +
                             "\"  has not been updated for LongitudinalDistanceAction\n";
        }
    }
}

}  // namespace OPENSCENARIO

/********************************************************************************
 * Copyright (c) 2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToMantle/ConvertScenarioLongitudinalDisplacement.h"

#include <stdexcept>

namespace OPENSCENARIO
{
LongitudinalDisplacement ConvertScenarioLongitudinalDisplacement(NET_ASAM_OPENSCENARIO::v1_1::LongitudinalDisplacement longitudinalDisplacement)
{
  switch (longitudinalDisplacement.GetFromLiteral(longitudinalDisplacement.GetLiteral()))
  {
    case NET_ASAM_OPENSCENARIO::v1_1::LongitudinalDisplacement::LEADING_REFERENCED_ENTITY:
      return mantle_api::Direction::kForward;
    case NET_ASAM_OPENSCENARIO::v1_1::LongitudinalDisplacement::TRAILING_REFERENCED_ENTITY:
      return mantle_api::Direction::kBackwards;
    case NET_ASAM_OPENSCENARIO::v1_1::LongitudinalDisplacement::ANY:
      // TODO: resolve the direction from the relative position of actor and reference entity
      return {};
  }
  throw std::runtime_error(
      "LongitudinalDistanceAction: resolve the given LongitudinalDisplacement to direction is not supported.");
}

}  // namespace OPENSCENARIO
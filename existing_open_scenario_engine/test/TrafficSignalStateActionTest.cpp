/*******************************************************************************
 * Copyright (c) 2022, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Storyboard/Actions/TrafficSignalStateAction.h"
#include "TestUtils.h"
#include "builders/ActionBuilder.h"
#include "builders/PositionBuilder.h"

#include <gtest/gtest.h>

namespace OPENSCENARIO
{
using namespace units::literals;

class TrafficSignalStateActionTest : public OpenScenarioEngineLibraryTestBase
{
  protected:
    void SetUp() override { OpenScenarioEngineLibraryTestBase::SetUp(); }
};

TEST_F(TrafficSignalStateActionTest, GivenTrafficSignalStateAction_WhenStep_ThenNameAndStatePassedToEnvironment)
{
    std::string expected_state("off,off,constant");
    std::string expected_name("traffic_light");

    using namespace OPENSCENARIO::TESTING;
    auto fake_traffic_signal_state_action =
        FakeTrafficSignalStateActionBuilder{}.WithState(expected_state).WithName(expected_name).Build();

    OPENSCENARIO::TrafficSignalStateAction traffic_signal_state_action{fake_traffic_signal_state_action, env_};

    EXPECT_CALL(*env_, SetTrafficSignalState(testing::Eq(expected_name), testing::Eq(expected_state))).Times(1);

    traffic_signal_state_action.Start();
    EXPECT_NO_THROW(traffic_signal_state_action.Step());
}

}  // namespace OPENSCENARIO

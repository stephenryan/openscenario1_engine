/********************************************************************************
 * Copyright (c) 2021-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>

#include "Conversion/OscToMantle/ConvertScenarioCoordinateSystem.h"
#include "Conversion/OscToMantle/ConvertScenarioRelativeDistanceType.h"
#include "Conversion/OscToMantle/ConvertScenarioRule.h"
#include "Conversion/OscToMantle/ConvertScenarioTimeToCollisionConditionTarget.h"
#include "Utils/EntityBroker.h"

namespace OpenScenarioEngine::v1_1
{
class TimeToCollisionCondition
{
public:
  struct Values
  {
    std::string triggeringEntity;
    bool alongRoute;
    bool freespace;
    OPENSCENARIO::TimeToCollisionConditionTarget timeToCollisionConditionTarget;
    OPENSCENARIO::CoordinateSystem coordinateSystem;
    OPENSCENARIO::RelativeDistanceType relativeDistanceType;
    OPENSCENARIO::Rule<double> rule;
  };
  struct Interfaces
  {
    std::shared_ptr<mantle_api::IEnvironment> environment;
  };

  TimeToCollisionCondition(Values values, Interfaces interfaces)
      : values{std::move(values)},
        mantle{interfaces} {};

  bool IsSatisfied() const;

private:
  Values values;
  Interfaces mantle;

  /// @brief Calculates the time to collision (TTC) between an
  ///        entity and a target position
  ///
  /// @param[in] entity   Entity of interest (normally the triggering)
  /// @param[in] pose     Pose to which the TTC shall be calculated
  /// @returns time to collision
  units::time::second_t CalculateTTC(
      const mantle_api::IEntity& entity,
      mantle_api::Pose targetPose) const;

  /// @brief Calculates the time to collision (TTC) between an
  ///        entity and a target position
  ///
  /// @param[in] entity        Entity of interest (normally the triggering)
  /// @param[in] targetEntity  Entity to which the TTC shall be calculated
  /// @returns time to collision
  units::time::second_t CalculateTTC(
      const mantle_api::IEntity& entity,
      const mantle_api::IEntity& targetEntity) const;
};

}  // namespace OpenScenarioEngine::v1_1
/*******************************************************************************
 * Copyright (c) 2021-2022, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "OpenScenarioEngine/OpenScenarioEngine.h"

#include "Constants.h"

#include <openScenarioLib/src/loader/FileResourceLocator.h>
#include <openScenarioLib/src/v1_1/loader/XmlScenarioImportLoaderFactoryV1_1.h>

#include <utility>

namespace OPENSCENARIO
{
namespace detail
{
std::pair<size_t, size_t> CountWarningsAndErrors(const OpenScenarioEngine::SimpleMessageLoggerPtr logger)
{
    const auto warnings = logger->GetMessagesFilteredByErrorLevel(NET_ASAM_OPENSCENARIO::ErrorLevel::WARNING);
    const auto errors = logger->GetMessagesFilteredByWorseOrEqualToErrorLevel(NET_ASAM_OPENSCENARIO::ErrorLevel::ERROR);
    return std::make_pair(errors.size(), warnings.size());
}

int SumAndPrintWarningsAndErrors(const OpenScenarioEngine::SimpleMessageLoggerPtr message_logger,
                                 const OpenScenarioEngine::SimpleMessageLoggerPtr catalog_logger)
{

    const auto message = detail::CountWarningsAndErrors(message_logger);
    const auto catalog = detail::CountWarningsAndErrors(catalog_logger);

    const auto total_errors = message.first + catalog.first;
    if (total_errors == 0)
    {
        std::cout << "Validation succeeded with 0 errors, " << message.second << " warnings, and " << catalog.second
                  << " catalog warnings \n";
    }
    else
    {
        std::cout << "Validation failed with " << message.first << " errors, " << catalog.first
                  << " catalog_errors with " << message.second << " warnings, and " << catalog.second
                  << " catalog_warnings \n";
    }
    return total_errors;
}
}  // namespace detail

OpenScenarioEngine::OpenScenarioEngine(const std::string& scenario_file_path,
                                       std::shared_ptr<mantle_api::IEnvironment> environment)
    : scenario_file_path_(scenario_file_path),
      environment_(environment),
      entity_creator_{environment},
      controller_creator_{std::make_shared<ControllerCreator>(environment)}
{
}

void OpenScenarioEngine::Init()
{
    if (!environment_)
    {
        throw std::runtime_error("Unable to initialize OpenScenarioEngine: No valid environment (nullptr)");
    }
    environment_->SetDefaultRoutingBehavior(mantle_api::DefaultRoutingBehavior::kRandomRoute);

    ParseScenarioFile();

    LoadRoadNetwork();

    CreateEntities();

    CreateControllers();

    CreateAndInitStoryboard();

    SetDefaultEnvironment();
}

mantle_api::ScenarioInfo OpenScenarioEngine::GetScenarioInfo() const
{
    mantle_api::ScenarioInfo info;

    info.description = scenario_data_ptr_->GetFileHeader()->GetDescription();
    info.scenario_timeout_duration = storyboard_->GetDuration();
    info.additional_information.emplace("full_scenario_path", ResolveScenarioPath(scenario_file_path_));

    if (scenario_definition_ptr_->GetCatalogLocations()->GetVehicleCatalog() != nullptr)
    {
        const auto& vehicle_catalog_path =
            scenario_definition_ptr_->GetCatalogLocations()->GetVehicleCatalog()->GetDirectory()->GetPath();
        info.additional_information.emplace("vehicle_catalog_path", vehicle_catalog_path);
    }

    if (scenario_definition_ptr_->GetRoadNetwork()->GetLogicFile() != nullptr)
    {
        info.additional_information.emplace(
            "full_map_path", ResolveMapPath(scenario_definition_ptr_->GetRoadNetwork()->GetLogicFile()->GetFilepath()));
    }
    return info;
}

void OpenScenarioEngine::Step()
{
    storyboard_->Step();
}

bool OpenScenarioEngine::IsFinished() const
{
    return storyboard_->IsFinished();
}

void OpenScenarioEngine::LoadScenarioData()
{
    const std::string resolved_scenario_file_path = ResolveScenarioPath(scenario_file_path_);
    message_logger_ =
        std::make_shared<NET_ASAM_OPENSCENARIO::SimpleMessageLogger>(NET_ASAM_OPENSCENARIO::ErrorLevel::INFO);
    catalog_message_logger_ =
        std::make_shared<NET_ASAM_OPENSCENARIO::SimpleMessageLogger>(NET_ASAM_OPENSCENARIO::ErrorLevel::INFO);
    auto loader_factory = NET_ASAM_OPENSCENARIO::v1_1::XmlScenarioImportLoaderFactory(catalog_message_logger_,
                                                                                      resolved_scenario_file_path);
    const auto loader = loader_factory.CreateLoader(std::make_shared<NET_ASAM_OPENSCENARIO::FileResourceLocator>());
    scenario_data_ptr_ = std::static_pointer_cast<NET_ASAM_OPENSCENARIO::v1_1::IOpenScenario>(
        loader->Load(message_logger_)->GetAdapter(typeid(NET_ASAM_OPENSCENARIO::v1_1::IOpenScenario).name()));
}

void OpenScenarioEngine::ParseScenarioFile()
{
    ValidateScenario();
    SetScenarioDefinitionPtr(scenario_data_ptr_);
}

int OpenScenarioEngine::ValidateScenario()
{
    LoadScenarioData();

    LogParsingMessages(message_logger_);
    LogParsingMessages(catalog_message_logger_);
    return detail::SumAndPrintWarningsAndErrors(message_logger_, catalog_message_logger_);
}

void OpenScenarioEngine::LogParsingMessages(SimpleMessageLoggerPtr message_logger)
{
    for (auto&& message :
         message_logger->GetMessagesFilteredByWorseOrEqualToErrorLevel(NET_ASAM_OPENSCENARIO::ErrorLevel::INFO))
    {
        auto text_marker = message.GetTextmarker();
        std::cout << NET_ASAM_OPENSCENARIO::ErrorLevelString::ToString(message.GetErrorLevel())
                  << ": (File:" << text_marker.GetFilename() << ") " << message.GetMsg() << " ("
                  << text_marker.GetLine() << "," << text_marker.GetColumn() << ") \n";
    }
}

void OpenScenarioEngine::LoadRoadNetwork()
{
    if (!scenario_definition_ptr_->GetRoadNetwork())
    {
        throw std::runtime_error("Scenario does not contain road network.");
    }

    auto road_network_ptr = scenario_definition_ptr_->GetRoadNetwork();

    // Check if, logic file is provided and load it via environment
    if (road_network_ptr->GetLogicFile() != nullptr)
    {
        mantle_api::MapDetails map_details;

        auto used_area = road_network_ptr->GetUsedArea();
        if (used_area != nullptr)
        {
            LoadUsedArea(used_area, map_details);
        }

        std::string resolved_map_path = ResolveMapPath(road_network_ptr->GetLogicFile()->GetFilepath());
        environment_->CreateMap(resolved_map_path, map_details);
    }

    // Check if, scenery file is provided and load it via environment
    if (road_network_ptr->GetSceneGraphFile() != nullptr)
    {
    }
}

void OpenScenarioEngine::LoadUsedArea(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IUsedArea> used_area,
                                      mantle_api::MapDetails& map_details)
{
    for (const auto& position : used_area->GetPositions())
    {
        const auto& geo_position = position->GetGeoPosition();
        mantle_api::LatLonPosition lat_lon_position{units::angle::radian_t(geo_position->GetLatitude()),
                                                    units::angle::radian_t(geo_position->GetLongitude())};
        map_details.map_region.push_back(lat_lon_position);
    }
}

void OpenScenarioEngine::CreateEntities()
{
    if (!scenario_definition_ptr_->GetEntities())
    {
        throw std::runtime_error("Scenario does not contain entities.");
    }

    // Add scenario objects to environment
    for (auto&& scenario_object : scenario_definition_ptr_->GetEntities()->GetScenarioObjects())
    {
        if (!scenario_object)
        {
            continue;
        }

        entity_creator_.CreateEntity(scenario_object);
    }

    // TODO: What to do with the entity selections?
}

void OpenScenarioEngine::CreateControllers()
{
    for (auto&& scenario_object : scenario_definition_ptr_->GetEntities()->GetScenarioObjects())
    {
        if (!scenario_object)
        {
            continue;
        }

        controller_creator_->CreateControllers(scenario_object);
    }
}

void OpenScenarioEngine::CreateAndInitStoryboard()
{
    if (!scenario_definition_ptr_->GetStoryboard())
    {
        throw std::runtime_error("Scenario does not contain storyboard.");
    }

    storyboard_ = std::make_unique<Storyboard>(*scenario_definition_ptr_->GetStoryboard(), environment_);
    storyboard_->Init();
}

void OpenScenarioEngine::SetDefaultEnvironment()
{
    environment_->SetDateTime(mantle_api::Time{43200000});
    environment_->SetWeather(GetDefaultWeather());
}

void OpenScenarioEngine::SetScenarioDefinitionPtr(OpenScenarioPtr open_scenario_ptr)
{
    if (!(open_scenario_ptr && open_scenario_ptr->GetOpenScenarioCategory() &&
          open_scenario_ptr->GetOpenScenarioCategory()->GetScenarioDefinition()))
    {
        throw std::runtime_error("Scenario file not found or file does not contain scenario definition.");
    }

    scenario_definition_ptr_ = open_scenario_ptr->GetOpenScenarioCategory()->GetScenarioDefinition();
}

void OpenScenarioEngine::ActivateExternalHostControl()
{
    controller_creator_->SetExternalControllerOverride();
}

}  // namespace OPENSCENARIO

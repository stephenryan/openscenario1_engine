
/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToMantle/ConvertScenarioTrajectoryRef.h"

#include "Conversion/OscToMantle/ConvertScenarioPosition.h"

namespace OPENSCENARIO
{
namespace detail
{
std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITrajectory> GetTrajectory(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITrajectoryRef> trajectoryRef)
{
  return trajectoryRef->GetTrajectory();
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITrajectory> ConvertCatalogReferenceToTrajectory(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ICatalogReference> catalogReference)
{
  auto catalogElement = catalogReference->GetRef();
  return NET_ASAM_OPENSCENARIO::v1_1::CatalogHelper::AsTrajectory(catalogElement);
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITrajectory> OpenScenarioToMantle(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITrajectoryRef> trajectoryRef,
                                                                               std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ICatalogReference> catalogReference)
{
  if (trajectoryRef) return GetTrajectory(trajectoryRef);
  if (catalogReference) return ConvertCatalogReferenceToTrajectory(catalogReference);
  throw std::runtime_error("convertScenarioRoute: Either trajectorReference or catalogReference must be set");
}
}  // namespace detail

TrajectoryRef ConvertScenarioTrajectoryRef(std::shared_ptr<mantle_api::IEnvironment> environment,
                                           std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITrajectoryRef> trajectoryRef,
                                           std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ICatalogReference> catalogReference)
{
  mantle_api::Trajectory trajectory{};

  auto follow_trajectory_action_trajectory = detail::OpenScenarioToMantle(trajectoryRef, catalogReference);

  trajectory.name = follow_trajectory_action_trajectory->GetName();

  auto follow_trajectory_action_trajectory_shape = follow_trajectory_action_trajectory->GetShape();

  if (follow_trajectory_action_trajectory_shape->GetPolyline())
  {
    mantle_api::PolyLine poly_line;

    for (auto poly_line_vertex : follow_trajectory_action_trajectory_shape->GetPolyline()->GetVertices())
    {
      mantle_api::PolyLinePoint poly_line_point;

      // TODO CK Separate method to parse position, which can be used anywhere in the openScenarioEngine
      auto poly_line_vertex_position = poly_line_vertex->GetPosition();
      auto pose = ConvertScenarioPosition(environment, poly_line_vertex_position);
      poly_line_point.pose = pose.value();
      // if (poly_line_vertex_position->GetWorldPosition())
      // {
      //     auto world_position = poly_line_vertex_position->GetWorldPosition();

      //     mantle_api::Vec3<units::length::meter_t> position;
      //     position.x = units::length::meter_t(world_position->GetX());
      //     position.y = units::length::meter_t(world_position->GetY());
      //     position.z = units::length::meter_t(world_position->GetZ());

      //     mantle_api::Orientation3<units::angle::radian_t> orientation;
      //     orientation.yaw = units::angle::radian_t(world_position->GetH());
      //     orientation.pitch = units::angle::radian_t(world_position->GetP());
      //     orientation.roll = units::angle::radian_t(world_position->GetR());

      //     poly_line_point.pose = {position, orientation};
      // }
      // else
      // {
      //     std::cout << "FollowTrajectoryAction currently does not support other Position types than "
      //                     "WorldPosition in Polyline shape";
      // }

      poly_line_point.time = units::time::second_t(poly_line_vertex->GetTime());

      poly_line.push_back(poly_line_point);
    }

    trajectory.type = poly_line;
    return trajectory;
  }
  return std::nullopt;
}

}  // namespace OPENSCENARIO
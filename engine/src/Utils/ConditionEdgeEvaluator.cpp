/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Utils/ConditionEdgeEvaluator.h"

namespace OPENSCENARIO
{
ConditionEdgeEvaluator::ConditionEdgeEvaluator(ConditionEdge conditionEdge)
    : update_evaluated_state_(EvalFunction(conditionEdge))
{
}

bool ConditionEdgeEvaluator::is_satisfied(bool state)
{
  update_evaluated_state_(state);
  last_state_ = state;
  return evaluated_state_;
}

std::function<void(bool)> ConditionEdgeEvaluator::EvalFunction(ConditionEdge conditionEdge)
{
  switch (conditionEdge)
  {
    case ConditionEdge::kRising:
      return [&](bool current_state) { evaluated_state_ = (!last_state_ && current_state); };
    case ConditionEdge::kFalling:
      return [&](bool current_state) { evaluated_state_ = (last_state_ && !current_state); };
    case ConditionEdge::kRisingOrFalling:
      return [&](bool current_state) { evaluated_state_ = (last_state_ != current_state); };
    case ConditionEdge::kNone:
      return [&](bool current_state) { evaluated_state_ = current_state; };
  }

  throw std::runtime_error("ConditionEdgeEvaluator: Invalid value for ConditionEdge");
}
}  // namespace OPENSCENARIO
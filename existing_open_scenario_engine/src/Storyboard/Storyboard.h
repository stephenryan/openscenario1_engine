/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "StateMachine/StateMachine.h"
#include "Story.h"
#include "Storyboard/Actions/Action.h"
#include "Storyboard/Conditions/Trigger.h"
// TODO: Remove, once stories can be instantiated
#include "Storyboard/Event.h"
#include "StoryboardElementStates/CompleteState.h"
#include "StoryboardElementStates/StoryboardRunningState.h"
#include "StoryboardElementStates/StoryboardStandbyState.h"

#include <MantleAPI/Common/time_utils.h>
#include <openScenarioLib/generated/v1_1/api/ApiClassInterfacesV1_1.h>

#include <iostream>

namespace OPENSCENARIO
{
class Storyboard
{
  public:
    explicit Storyboard(NET_ASAM_OPENSCENARIO::v1_1::IStoryboard& storyboard,
                        std::shared_ptr<mantle_api::IEnvironment> environment);

    void Init();

    void Step();

    bool IsFinished();

    mantle_api::Time GetDuration() const;

  protected:
    // TODO: Remove events, once stories can be instantiated
    std::vector<std::shared_ptr<Event>> events;
    std::vector<std::shared_ptr<Action>> init_actions;
    std::vector<std::shared_ptr<Story>> stories;
    std::unique_ptr<Trigger> stop_trigger_{nullptr};

  private:
    void CreateInitActions();
    void CreateStories();
    void CreateStopTriggerIfExists();

    NET_ASAM_OPENSCENARIO::v1_1::IStoryboard* data_{};
    std::shared_ptr<mantle_api::IEnvironment> environment_;

    bool is_finished_{true};

    // Alternative state machine approach
    StateMachine state_machine_;
    StoryboardStandbyState standby_state_;
    StoryboardRunningState running_state_;
    CompleteState complete_state_;
};

}  // namespace OPENSCENARIO

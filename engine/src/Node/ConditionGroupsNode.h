/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <agnostic_behavior_tree/composite_node.h>

namespace yase
{
/// Repeatedly execute all its conditions until one is fullfilled
class ConditionGroupsNode : public CompositeNode
{
public:
  /// Creates a conditions node
  explicit ConditionGroupsNode(const std::string &name);
  ~ConditionGroupsNode() override = default;
  void onInit() override;

private:
  NodeStatus tick() final;
};

}  // namespace yase

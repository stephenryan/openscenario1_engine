
/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToMantle/ConvertScenarioTrafficSignalController.h"

namespace OPENSCENARIO
{
TrafficSignalController ConvertScenarioTrafficSignalController(std::shared_ptr<NET_ASAM_OPENSCENARIO::INamedReference<NET_ASAM_OPENSCENARIO::v1_1::ITrafficSignalController>> trafficSignalControllerRef)
{
  //return trafficSignalControllerRef->GetNameRef();
  return trafficSignalControllerRef->GetTargetObject()->GetName();
}

}  // namespace OPENSCENARIO
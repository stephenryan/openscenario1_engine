/********************************************************************************
 * Copyright (c) 2022, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Utils/EntityUtils.h"

#include <iostream>

namespace OPENSCENARIO
{
using namespace units::literals;

std::vector<mantle_api::Vec3<units::length::meter_t>> EntityUtils::GetBoundingBoxCornerPoints(
    const mantle_api::IEntity& ref_entity)
{
  const auto bounding_box = ref_entity.GetProperties()->bounding_box;
  const auto half_length = bounding_box.dimension.length * 0.5;
  const auto half_width = bounding_box.dimension.width * 0.5;
  const auto half_height = bounding_box.dimension.height * 0.5;

  return {{half_length, half_width, -half_height},
          {half_length, -half_width, -half_height},
          {half_length, half_width, half_height},
          {half_length, -half_width, half_height},
          {-half_length, half_width, -half_height},
          {-half_length, -half_width, -half_height},
          {-half_length, half_width, half_height},
          {-half_length, -half_width, half_height}};
}

std::vector<mantle_api::Vec3<units::length::meter_t>> EntityUtils::GetBoundingBoxCornerPointsInGlobal(
    const std::shared_ptr<mantle_api::IEnvironment> environment,
    const mantle_api::Vec3<units::length::meter_t>& position,
    const mantle_api::Orientation3<units::angle::radian_t>& orientation,
    const std::vector<mantle_api::Vec3<units::length::meter_t>>& local_corner_points)
{
  std::vector<mantle_api::Vec3<units::length::meter_t>> corner_points;
  corner_points.reserve(local_corner_points.size());
  std::transform(local_corner_points.begin(),
                 local_corner_points.end(),
                 std::back_insert_iterator(corner_points),
                 [environment, position, orientation](const auto& point) {
                   return environment->GetGeometryHelper()->TranslateGlobalPositionLocally(
                       position, orientation, point);
                 });

  return corner_points;
}

std::vector<mantle_api::Vec3<units::length::meter_t>>
EntityUtils::GetCornerPositionsInLocalSortedByLongitudinalDistance(
    const std::shared_ptr<mantle_api::IEnvironment> environment,
    const mantle_api::IEntity& entity,
    const mantle_api::Vec3<units::length::meter_t>& local_origin,
    const mantle_api::Orientation3<units::angle::radian_t>& local_orientation)
{
  const auto corner_points_global = GetBoundingBoxCornerPointsInGlobal(
      environment, entity.GetPosition(), entity.GetOrientation(), GetBoundingBoxCornerPoints(entity));

  auto corner_points_local = environment->GetGeometryHelper()->TransformPolylinePointsFromWorldToLocal(
      corner_points_global, local_origin, local_orientation);

  std::sort(
      corner_points_local.begin(), corner_points_local.end(), [](const auto& a, const auto& b) { return a.x < b.x; });

  return corner_points_local;
}

units::length::meter_t EntityUtils::CalculateLongitudinalFreeSpaceDistance(
    const std::shared_ptr<mantle_api::IEnvironment> environment,
    const mantle_api::IEntity& master_entity,
    const mantle_api::IEntity& reference_entity)
{
  const auto local_entity_corner_points = GetBoundingBoxCornerPoints(reference_entity);
  const auto global_entity_corner_points = GetBoundingBoxCornerPointsInGlobal(
      environment, reference_entity.GetPosition(), reference_entity.GetOrientation(), local_entity_corner_points);
  auto relative_entity_corner_points = environment->GetGeometryHelper()->TransformPolylinePointsFromWorldToLocal(
      global_entity_corner_points, master_entity.GetPosition(), master_entity.GetOrientation());

  auto sort_points_longitudinally = [](const auto& a, const auto& b) { return a.x < b.x; };

  std::sort(relative_entity_corner_points.begin(), relative_entity_corner_points.end(), sort_points_longitudinally);
  const auto entity_min_x = relative_entity_corner_points[0].x;
  const auto entity_max_x = relative_entity_corner_points[relative_entity_corner_points.size() - 1].x;

  auto master_entity_corner_points = GetBoundingBoxCornerPoints(master_entity);
  std::sort(master_entity_corner_points.begin(), master_entity_corner_points.end(), sort_points_longitudinally);
  const auto master_min_x = master_entity_corner_points[0].x;
  const auto master_max_x = master_entity_corner_points[master_entity_corner_points.size() - 1].x;

  if ((master_min_x > entity_min_x && master_min_x < entity_max_x) ||
      (master_max_x > entity_min_x && master_max_x < entity_max_x))
  {
    std::cout << "Warning: in CalculateLongitudinalFreeSpaceDistance Entity \"" << master_entity.GetName()
              << "\" and \"" << reference_entity.GetName() << "\" are overlapping in the x-axis" << std::endl;
    return units::length::meter_t{0.0};
  }

  const auto minimum_distance = std::min({std::abs(master_min_x.value() - entity_min_x.value()),
                                          std::abs(master_min_x.value() - entity_max_x.value()),
                                          std::abs(master_max_x.value() - entity_min_x.value()),
                                          std::abs(master_max_x.value() - entity_max_x.value())});
  return units::length::meter_t{minimum_distance};
}

units::length::meter_t EntityUtils::CalculateRelativeLongitudinalDistance(
    const std::shared_ptr<mantle_api::IEnvironment> environment,
    const mantle_api::IEntity& master_entity,
    const mantle_api::IEntity& reference_entity)

{
  const auto master_entity_origin_global_position = environment->GetGeometryHelper()->TranslateGlobalPositionLocally(
      master_entity.GetPosition(),
      master_entity.GetOrientation(),
      -master_entity.GetProperties()->bounding_box.geometric_center);

  const auto reference_entity_origin_global_position =
      environment->GetGeometryHelper()->TranslateGlobalPositionLocally(
          reference_entity.GetPosition(),
          reference_entity.GetOrientation(),
          -reference_entity.GetProperties()->bounding_box.geometric_center);

  const auto ref_entity_origin_local_position = environment->GetGeometryHelper()->TransformPositionFromWorldToLocal(
      reference_entity_origin_global_position, master_entity_origin_global_position, master_entity.GetOrientation());

  return units::math::abs(ref_entity_origin_local_position.x);
}

mantle_api::IEntity& EntityUtils::GetEntityByName(const std::shared_ptr<mantle_api::IEnvironment>& environment,
                                                  const std::string& entity_name)
{
  if (auto entity = environment->GetEntityRepository().Get(entity_name); entity)
  {
    return entity.value().get();
  }
  throw std::runtime_error("EntityUtils: Entity with name \"" + entity_name +
                           "\" does not exist. Please adjust the scenario.");
}

}  // namespace OPENSCENARIO

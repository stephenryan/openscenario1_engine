/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Test/test_utils.h>
#include <gtest/gtest.h>

#include "Storyboard/GenericAction/DeleteEntityAction_impl.h"

TEST(DeleteEntityActionTest, GivenEntity_ThenDeleteEntity)
{
  auto mockEnvironment = std::make_shared<mantle_api::MockEnvironment>();

  OpenScenarioEngine::v1_1::DeleteEntityAction deleteEntityAction({},
                                                                  {mockEnvironment});

  ASSERT_TRUE(deleteEntityAction.Step());
}
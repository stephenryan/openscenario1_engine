/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Node/PrivateNode.h"

#include <openScenarioLib/generated/v1_1/api/ApiClassInterfacesV1_1.h>

#include "Conversion/OscToMantle/ConvertScenarioEntity.h"
#include "Conversion/OscToNode/ParsePrivateAction.h"
#include "Utils/EntityBroker.h"

PrivateNode::PrivateNode(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IPrivate> private_)
    : yase::ParallelNode{"PrivateActions"}
{
  entityBroker_ = std::make_shared<EntityBroker>();
  entityBroker_->add(OPENSCENARIO::ConvertScenarioEntity(private_->GetEntityRef()));
  for (const auto &privateAction : private_->GetPrivateActions())
  {
    addChild(parse(privateAction));
  }
}

void PrivateNode::lookupAndRegisterData(yase::Blackboard &blackboard)
{
  blackboard.set("EntityBroker", entityBroker_);
}

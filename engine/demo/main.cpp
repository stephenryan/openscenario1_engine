/********************************************************************************
 * Copyright (c) 2021-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Test/test_utils.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <iostream>
#include <string>

#include "OpenScenarioEngine/OpenScenarioEngine.h"

using namespace units::literals;

using testing::_;
using testing::A;
using testing::Invoke;
using testing::NiceMock;
using testing::Return;
using testing::ReturnRef;

int main(int argc, char* argv[])
{
  if (argc < 2)
  {
    std::cout << "Usage: ./openScenarioEngine <path>/scenario.xosc\n";
    return 0;
  }

  auto mockEnvironment = std::make_shared<mantle_api::MockEnvironment>();

  auto& mockEntityRepository = static_cast<mantle_api::MockEntityRepository&>(mockEnvironment->GetEntityRepository());

  mantle_api::MockVehicle mockVehicle1;
  ON_CALL(mockEntityRepository, Create("Vehicle1", A<const mantle_api::VehicleProperties&>())).WillByDefault(ReturnRef(mockVehicle1));

  mantle_api::MockVehicle mockVehicle2;
  ON_CALL(mockEntityRepository, Create("Vehicle2", A<const mantle_api::VehicleProperties&>())).WillByDefault(ReturnRef(mockVehicle2));

  auto& mockControllerRepository = mockEnvironment->GetControllerRepository();

  mantle_api::MockController mockController;
  ON_CALL(mockControllerRepository, Create(_)).WillByDefault(ReturnRef(mockController));

  std::string scenario_file{argv[1]};
  OPENSCENARIO::OpenScenarioEngine openScenarioEngine(scenario_file, mockEnvironment);

  mantle_api::Time current_time{0_s};
  ON_CALL(*mockEnvironment, GetSimulationTime())
      .WillByDefault(Invoke([&current_time]() { return current_time; }));
  ON_CALL(*mockEnvironment, HasControlStrategyGoalBeenReached(_, mantle_api::ControlStrategyType::kFollowVelocitySpline))
      .WillByDefault(Invoke([&current_time]() -> bool { return current_time > 5.5_s; }));

  openScenarioEngine.Init();
  while (!openScenarioEngine.IsFinished())
  {
    openScenarioEngine.Step();
#ifndef NDEBUG
    openScenarioEngine.Print();
#endif
    current_time += 500_ms;
  }
}

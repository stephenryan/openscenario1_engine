################################################################################
# Copyright (c) 2021-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

from dataclasses import dataclass
from typing import Dict, List, Tuple, Union
from caseconverter import pascalcase, camelcase
from enum import IntFlag


class GeneratorUtils():
    MODEL_VERSION = 'v1_1'
    keywords = ["private"]

    @staticmethod
    def _unpack(var: Union[str, object]) -> str:
        if isinstance(var, str):
            return var
        return var.name

    @staticmethod
    def getter(var: Union[str, object], no_brackets=False) -> str:
        return f'Get{pascalcase(GeneratorUtils._unpack(var))}' + ('' if no_brackets else '()')

    @staticmethod
    def tag(var):
        return pascalcase(GeneratorUtils._unpack(var))

    @classmethod
    def local(cls, var: Union[str, object]):
        var = camelcase(GeneratorUtils._unpack(var))
        return var + '_' if var in cls.keywords else var

    @staticmethod
    def member(var):
        var = camelcase(GeneratorUtils._unpack(var))
        return var + '_' if var[-1] != '_' else var

    @classmethod
    def interface(cls, type: str, is_list):
        if is_list:
            return f'std::vector<std::shared_ptr<NET_ASAM_OPENSCENARIO::{cls.MODEL_VERSION}::I{pascalcase(type)}>>'
        else:
            return f'std::shared_ptr<NET_ASAM_OPENSCENARIO::{cls.MODEL_VERSION}::I{pascalcase(type)}>'


class ChildType(IntFlag):
    MAPPED = 64
    CHOICE = 32
    LIST = 16
    ENUM = 8
    INTERFACE = 4
    MODEL = 2
    PRIMITIVE = 1


class Datatype():
    stl_type_mapping = {
        'string': 'std::string',
        'boolean': 'bool',
        'double': 'double',
        'dateTime': 'std::string',
        "unsignedInt": "unsigned int",
        "unsignedShort": "unsigned int"
    }

    stl_includes = {
        'std::string': '<string>'
    }

    def __init__(self, name, type=None, is_list=False) -> None:
        self.name = pascalcase(name)
        self.is_list = is_list
        type = type if type else self.name
        self.raw_type = type if type else self.name

        if self.raw_type in Datatype.stl_type_mapping:
            self.type = Datatype.stl_type_mapping[type]
            self._include = Datatype.stl_includes.get(self.type, None)
            self._mapped = True
        else:
            self.type = GeneratorUtils.interface(type, is_list)
            self._include = self.name if is_list else self.raw_type
            self._mapped = False
        self.callee = None
        self.declaration = f'{self.type} {GeneratorUtils.local(self)}'

    def set_callee(self, callee):
        self.callee = callee

    def include(self, prefix=""):
        if self._mapped:
            return self._include
        else:
            return f'"{prefix}{pascalcase(self._include)}.h"'

    def __str__(self):
        return f'{GeneratorUtils.member(self.callee)}->{GeneratorUtils.getter(self.name)}'


class UmlClass():

    def __init__(self, model: dict, node: str) -> None:
        self.this = Datatype(node)
        self.name = node

        properties = model[node]['properties']
        self.has_start_trigger = 'startTrigger' in properties
        self.has_stop_trigger = 'stopTrigger' in properties

        self._children = self._parse_children(properties)

    @staticmethod
    def _getChildType(p):
        if p['type.isPrimitiveType']:
            return ChildType.PRIMITIVE

        child_type = 0
        if p['isList']:
            child_type |= ChildType.LIST
        if p['isXorElement']:
            child_type |= ChildType.CHOICE

        if p['type.isEnumeration']:
            return child_type | ChildType.ENUM

        if p['type.name'] in ['CatalogElement', 'Entity', 'SpawnedObject', 'StoryboardElement']:
            return child_type | ChildType.INTERFACE

        return child_type | ChildType.MODEL

    @staticmethod
    def _parse_children(properties) -> Tuple[Dict[str, Datatype], Dict[ChildType, Datatype]]:
        children = []
        exclude_types = []   # ['Trigger']
        exclude_properties = []  # ['isEnumeration']

        for name, p in properties.items():
            if not any([p[f'type.{exclude}'] for exclude in exclude_properties]) and p['type.name'] not in exclude_types:
                children.append((UmlClass._getChildType(
                    p), Datatype(name, p['type.name'], p['isList'])))
        return children

    def children(self, select: ChildType = None) -> List[Datatype]:
        if select:
            return [c for _, c in filter(lambda entry: select in entry[0], self._children)]
        else:
            return [c for _, c in self._children]


def is_leaf(uml_class, converters):
    for child in uml_class.children(ChildType.MODEL):
        if child.raw_type not in converters:
            return False
    return True


def parse_mappings(uml_class, converters, dependencies):
    mappings = {}
    for child in uml_class.children(ChildType.MODEL):
        if child.raw_type in converters:
            mappings[child.name] = Converter(
                dependencies, child.name, child.raw_type, **converters[child.raw_type])

    for child in uml_class.children(ChildType.INTERFACE):
        if child.raw_type in converters:
            mappings[child.name] = Converter(
                dependencies, child.name, child.raw_type, **converters[child.raw_type])

    for child in uml_class.children(ChildType.ENUM):
        if child.raw_type in converters:
            mappings[child.name] = Converter(
                dependencies, child.name, child.raw_type, **converters[child.raw_type])

    return mappings


@dataclass(frozen=True)
class Dependency:
    name: str
    type: str
    include: str


class Converter:
    def __init__(self, dependency_mapping, child_name, raw_type, converter, return_type, include, type, dependencies, consumes=None):
        self.name = child_name
        self.raw_type = raw_type
        self.converter = converter
        self.consumes = [pascalcase(c) for c in consumes] if consumes else []
        self.include = include
        self.converter_type = type
        self.dependencies = [Dependency(
            **dependency_mapping[d]) for d in dependencies]

        self.type = self.resolve_return_type(return_type)
        self.callee = None
        self.consumed_attributes = []
        self.declaration = f"{self.type} {GeneratorUtils.local(self) if self.converter_type == 'static' else GeneratorUtils.getter(self, no_brackets=True)}"

    def resolve_return_type(self, return_type):
        if self.converter_type == 'static':
            return return_type
        return f'std::function<{return_type}()>'

    def set_callee(self, callee):
        self.callee = callee

    def is_consumed(self, attribute):
        if attribute.name in self.consumes:
            self.consumed_attributes.append(attribute.name)
            return True
        return False

    def wrap_in_lambda(self, call):
        return f'[=](){{ return {call}; }}'

    def __str__(self):
        if not self.callee:
            raise Exception("Unable to create converter: callee not set")

        base_class_getter = f'{GeneratorUtils.member(self.callee)}->{GeneratorUtils.getter(self.name)}'

        variables = [GeneratorUtils.local(d) for d in self.dependencies] + \
            [base_class_getter] + \
            [f'{GeneratorUtils.member(self.callee)}->{GeneratorUtils.getter(a)}' for a in self.consumed_attributes]
        call = f'{self.converter}({", ".join(variables)})'
        return call if self.converter_type == 'static' else self.wrap_in_lambda(call)


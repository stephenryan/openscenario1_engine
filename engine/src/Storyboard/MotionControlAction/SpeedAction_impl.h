/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include "Storyboard/MotionControlAction/SpeedAction_base.h"

namespace OpenScenarioEngine::v1_1
{
class SpeedAction : public SpeedActionBase
{
public:
  SpeedAction(Values values, Interfaces interfaces)
      : SpeedActionBase{std::move(values), interfaces} {};

  void SetControlStrategy() override;
  bool HasControlStrategyGoalBeenReached(const std::string &actor) override;
  mantle_api::MovementDomain GetMovementDomain() const override;

private:
  void SetSpline(const mantle_api::IEntity &entity,
                 mantle_api::SplineSection<units::velocity::meters_per_second> spline_section,
                 units::velocity::meters_per_second_t target_speed);
  void SetLinearVelocitySplineControlStrategy(const std::string &actor);
};

}  // namespace OpenScenarioEngine::v1_1
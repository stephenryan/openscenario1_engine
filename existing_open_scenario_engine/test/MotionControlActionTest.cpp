/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Storyboard/Actions/MotionControlAction.h"
#include "TestUtils.h"

#include <gmock/gmock.h>

namespace OPENSCENARIO
{

class MockLongitudinalMotionControlAction : public MotionControlAction
{
  public:
    MockLongitudinalMotionControlAction(std::shared_ptr<mantle_api::IEnvironment> environment,
                                        std::vector<std::string> actors)
        : MotionControlAction(environment, actors)
    {
    }

  private:
    virtual void StartAction() {}
    virtual bool HasControlStrategyGoalBeenReached(const std::string&) { return true; }
    virtual bool IsLongitudinal() { return true; }
    virtual bool IsLateral() { return false; }
};

class MockLateralMotionControlAction : public MotionControlAction
{
  public:
    MockLateralMotionControlAction(std::shared_ptr<mantle_api::IEnvironment> environment,
                                   std::vector<std::string> actors)
        : MotionControlAction(environment, actors)
    {
    }
    MOCK_METHOD(bool, HasControlStrategyGoalBeenReached, (const std::string&), (override));

  private:
    virtual void StartAction() {}
    virtual bool IsLongitudinal() { return false; }
    virtual bool IsLateral() { return true; }
};

class MotionControlActionTestFixture : public OpenScenarioEngineLibraryTestBase
{
  protected:
    void SetUp() override { OpenScenarioEngineLibraryTestBase::SetUp(); }
};

TEST_F(MotionControlActionTestFixture, GivenLongitudinalAction_WhenControlStrategyGoalReached_ThenResetLongitudinal)
{
    MockLongitudinalMotionControlAction mock_action(env_, std::vector<std::string>{"Ego"});

    auto expected_control_strategy = mantle_api::KeepVelocityControlStrategy();

    EXPECT_CALL(*env_, UpdateControlStrategies(0, testing::_))
        .Times(1)
        .WillRepeatedly([expected_control_strategy](
                            std::uint64_t controller_id,
                            std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies) {
            std::ignore = controller_id;
            EXPECT_EQ(1, control_strategies.size());
            auto control_strategy = dynamic_cast<mantle_api::KeepVelocityControlStrategy*>(control_strategies[0].get());
            EXPECT_NE(nullptr, control_strategy);
            EXPECT_EQ(*control_strategy, expected_control_strategy);
        });

    mock_action.Start();
    mock_action.Step();
}

TEST_F(MotionControlActionTestFixture, GivenLateralAction_WhenControlStrategyGoalReached_ThenResetLateral)
{
    MockLateralMotionControlAction mock_action(env_, std::vector<std::string>{"Ego"});
    EXPECT_CALL(mock_action, HasControlStrategyGoalBeenReached("Ego")).WillOnce(::testing::Return(true));

    auto expected_control_strategy = mantle_api::KeepLaneOffsetControlStrategy();

    EXPECT_CALL(*env_, UpdateControlStrategies(0, testing::_))
        .Times(1)
        .WillRepeatedly(
            [expected_control_strategy](std::uint64_t controller_id,
                                        std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies) {
                std::ignore = controller_id;
                EXPECT_EQ(1, control_strategies.size());
                auto control_strategy =
                    dynamic_cast<mantle_api::KeepLaneOffsetControlStrategy*>(control_strategies[0].get());
                EXPECT_NE(nullptr, control_strategy);
                EXPECT_EQ(*control_strategy, expected_control_strategy);
            });

    mock_action.Start();
    mock_action.Step();
}

TEST_F(MotionControlActionTestFixture,
       GivenLateralAction_WhenHasControlStrategyGoalBeenReachedThrowException_ThenActionIsComplete)
{
    MockLateralMotionControlAction mock_action(env_, std::vector<std::string>{"Ego"});
    EXPECT_CALL(mock_action, HasControlStrategyGoalBeenReached("Ego")).WillOnce(testing::Throw(std::exception{}));
    auto expected_control_strategy = mantle_api::KeepLaneOffsetControlStrategy();

    mock_action.Start();
    mock_action.Step();
    EXPECT_TRUE(mock_action.isComplete());
}

}  // namespace OPENSCENARIO

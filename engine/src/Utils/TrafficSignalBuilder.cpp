/********************************************************************************
 * Copyright (c) 2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Utils/TrafficSignalBuilder.h"

#include <agnostic_behavior_tree/composite/sequence_node.h>
#include <agnostic_behavior_tree/utils/condition.h>
#include <units.h>

#include <memory>
#include <numeric>

#include "Node/RepeatingSequenceNode.h"
#include "Node/TrafficSignalPhaseNode.h"
#include "Utils/EngineAbortFlags.h"

using namespace units::literals;

class EngineAbortCondition : public yase::Condition
{
public:
  explicit EngineAbortCondition()
      : Condition("EngineAbortCondition"){};

  void lookupAndRegisterData(const yase::Blackboard& blackboard) final
  {
    engine_abort_flags_ = blackboard.get<std::shared_ptr<EngineAbortFlags>>("EngineAbortFlags");
  }

  void onInit() final{};

  bool evaluate() final
  {
    return *engine_abort_flags_ != EngineAbortFlags::kNoAbort;
  };

  std::shared_ptr<EngineAbortFlags> engine_abort_flags_;
};

namespace OPENSCENARIO
{
void TrafficSignalBuilder::Add(const TrafficSignalController& controller)
{
  auto controller_node = std::make_shared<yase::RepeatingSequenceNode>(controller.name);
  auto total_period = 0_s;
  for (const auto& phase : controller.phases)
  {
    total_period += phase.duration;
  }
  auto trigger_time{0_s};
  for (const auto& phase : controller.phases)
  {
    controller_node->addChild(std::make_shared<yase::TrafficSignalPhaseNode>(
        phase.name,
        total_period,
        trigger_time,
        phase.duration,
        phase.traffic_signal_states));
    trigger_time += phase.duration;
  }
  traffic_signals_->addChild(controller_node);
}

std::shared_ptr<yase::StopAtNode> TrafficSignalBuilder::Create()
{
  auto root = std::make_shared<yase::StopAtNode>(std::make_unique<EngineAbortCondition>());
  root->setChild(traffic_signals_);
  return root;
}

}  // namespace OPENSCENARIO

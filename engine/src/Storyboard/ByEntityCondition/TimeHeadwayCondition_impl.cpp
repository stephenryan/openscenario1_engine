/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Storyboard/ByEntityCondition/TimeHeadwayCondition_impl.h"

#include "Utils/Constants.h"
#include "Utils/EntityUtils.h"

namespace OpenScenarioEngine::v1_1
{
bool TimeHeadwayCondition::IsSatisfied() const
{
  const auto& ref_entity = OPENSCENARIO::EntityUtils::GetEntityByName(mantle.environment, values.entityRef);
  const auto& trigger_entity = OPENSCENARIO::EntityUtils::GetEntityByName(mantle.environment, values.triggeringEntity);

  units::length::meter_t distance{};
  if (values.coordinateSystem == OPENSCENARIO::CoordinateSystem::kEntity)
  {
    distance = values.freespace
                   ? OPENSCENARIO::EntityUtils::CalculateLongitudinalFreeSpaceDistance(mantle.environment, trigger_entity, ref_entity)
                   : OPENSCENARIO::EntityUtils::CalculateRelativeLongitudinalDistance(mantle.environment, trigger_entity, ref_entity);
  }
  else if (values.coordinateSystem == OPENSCENARIO::CoordinateSystem::kLane)
  {
    const auto longitudinal_lane_distance =
        mantle.environment->GetQueryService().GetLongitudinalLaneDistanceBetweenPositions(trigger_entity.GetPosition(),
                                                                                          ref_entity.GetPosition());
    if (!longitudinal_lane_distance.has_value())
    {
      throw std::runtime_error(
          "TimeHeadwayCondition: CoordinateSystem is set to \"LANE\", but can not get the longitudinal distance "
          "of the reference and the triggering entities along the lane center line. Please adjust scenario.");
    }
    distance = longitudinal_lane_distance.value();

    if (values.freespace)
    {
      const auto trigger_entity_lane_orientation =
          mantle.environment->GetQueryService().GetLaneOrientation(trigger_entity.GetPosition());
      const auto trigger_entity_corners_along_lane =
          OPENSCENARIO::EntityUtils::GetCornerPositionsInLocalSortedByLongitudinalDistance(
              mantle.environment, trigger_entity, trigger_entity.GetPosition(), trigger_entity_lane_orientation);

      const auto ref_entity_lane_pose = mantle.environment->GetQueryService().FindLanePoseAtDistanceFrom(
          {trigger_entity.GetPosition(), trigger_entity.GetOrientation()},
          distance,
          mantle_api::Direction::kForward);
      const auto ref_entity_corners_along_lane =
          OPENSCENARIO::EntityUtils::GetCornerPositionsInLocalSortedByLongitudinalDistance(
              mantle.environment, ref_entity, ref_entity.GetPosition(), ref_entity_lane_pose.value().orientation);

      distance = distance - (units::math::abs(trigger_entity_corners_along_lane.front().x) +
                             units::math::abs(ref_entity_corners_along_lane.back().x));
    }
  }
  else
  {
    // should never happen
    throw std::runtime_error("TimeHeadwayCondition: the given CoordinateSystem is not supported.");
  }

  const auto trigger_entity_velocity = trigger_entity.GetVelocity().Length();
  const auto time_headway = trigger_entity_velocity > OPENSCENARIO::limits::kAcceptableVelocityMin
                                ? distance / trigger_entity_velocity
                                : OPENSCENARIO::limits::kTimeHeadwayMax;

  return values.rule.IsSatisfied(time_headway.value());
}

}  // namespace OpenScenarioEngine::v1_1
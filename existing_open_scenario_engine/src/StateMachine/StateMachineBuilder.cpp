/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG),
 *                     in-tech GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "StateMachineBuilder.h"

#include "Storyboard/Actions/AssignRouteAction.h"
#include "Storyboard/Actions/CustomCommandAction.h"
#include "Storyboard/Actions/FollowTrajectoryAction.h"
#include "Storyboard/Actions/LaneChangeAction.h"
#include "Storyboard/Actions/LongitudinalDistanceAction.h"
#include "Storyboard/Actions/SpeedAction.h"
#include "Storyboard/Actions/TeleportAction.h"
#include "Storyboard/Actions/TrafficSignalStateAction.h"
#include "Storyboard/Actions/VisibilityAction.h"

namespace OPENSCENARIO
{

void StateMachineBuilder::parseStoryboard(/*const*/ osc::IStoryboard& storyboard)
{
    if (const auto& init = storyboard.GetInit(); init)
    {
        if (const auto& actions = storyboard.GetInit()->GetActions(); actions)
        {
            for (const auto& global_action : actions->GetGlobalActions())
            {
                parseGlobalAction(*global_action.get());
            }
            for (const auto& user_defined_action : actions->GetUserDefinedActions())
            {
                parseUserDefinedAction(*user_defined_action.get());
            }
            for (const auto& private_ : actions->GetPrivates())
            {
                for (const auto& private_action : private_->GetPrivateActions())
                {
                    parsePrivateAction(*private_action.get());
                }
            }
        }
    }
}

void StateMachineBuilder::parseGlobalAction(/*const*/ osc::IGlobalAction& global_action)
{
    if (const auto& environment_action = global_action.GetEnvironmentAction(); environment_action)
    {
        parseAction(*environment_action.get());
    }
    if (const auto& entity_action = global_action.GetEntityAction(); entity_action)
    {
        if (const auto& add_entity_action = entity_action->GetAddEntityAction(); add_entity_action)
        {
            parseAction(*add_entity_action.get());
        }
        if (const auto& delete_entity_action = entity_action->GetDeleteEntityAction(); delete_entity_action)
        {
            parseAction(*delete_entity_action.get());
        }
    }
    if (const auto& parameter_action = global_action.GetParameterAction(); parameter_action)
    {
        if (const auto& parameter_set_action = parameter_action->GetSetAction(); parameter_set_action)
        {
            parseAction(*parameter_set_action.get());
        }
        if (const auto& parameter_modify_action = parameter_action->GetModifyAction(); parameter_modify_action)
        {
            parseAction(*parameter_modify_action.get());
        }
    }
    if (const auto& infrastructure_action = global_action.GetInfrastructureAction(); infrastructure_action)
    {
        if (const auto& traffic_signal_action = infrastructure_action->GetTrafficSignalAction(); traffic_signal_action)
        {
            if (const auto& trafficSignalController_action = traffic_signal_action->GetTrafficSignalControllerAction();
                trafficSignalController_action)
            {
                parseAction(*trafficSignalController_action.get());
            }
            if (const auto& trafficSignalState_action = traffic_signal_action->GetTrafficSignalStateAction();
                trafficSignalState_action)
            {
                parseAction(*trafficSignalState_action.get());
            }
        }
    }
    if (const auto& traffic_action = global_action.GetTrafficAction(); traffic_action)
    {
        if (const auto& traffic_source_action = traffic_action->GetTrafficSourceAction(); traffic_source_action)
        {
            parseAction(*traffic_source_action.get());
        }
        if (const auto& traffic_sink_action = traffic_action->GetTrafficSinkAction(); traffic_sink_action)
        {
            parseAction(*traffic_sink_action.get());
        }
        if (const auto& traffic_swarm_action = traffic_action->GetTrafficSwarmAction(); traffic_swarm_action)
        {
            parseAction(*traffic_swarm_action.get());
        }
    }
}

void StateMachineBuilder::parseUserDefinedAction(/*const*/ osc::IUserDefinedAction& user_defined_action)
{
    std::ignore = user_defined_action;
}

void StateMachineBuilder::parsePrivateAction(/*const*/ osc::IPrivateAction& private_action)
{
    std::ignore = private_action;
}

std::shared_ptr<Action> StateMachineBuilder::CreatePrivateAction(std::shared_ptr<osc::IPrivateAction> private_action,
                                                                 std::shared_ptr<mantle_api::IEnvironment> environment,
                                                                 std::vector<std::string> actors)
{
    // TODO: add more actions (LaneChangeAction, LaneOffsetAction)
    if (private_action->GetTeleportAction())
    {
        return std::make_shared<TeleportAction>(private_action->GetTeleportAction(), environment, actors);
    }
    else if (private_action->GetLongitudinalAction())
    {
        auto longitudinal_action = private_action->GetLongitudinalAction();
        if (longitudinal_action->GetSpeedAction())
        {
            return std::make_shared<SpeedAction>(longitudinal_action->GetSpeedAction(), environment, actors);
        }
        else if (longitudinal_action->GetLongitudinalDistanceAction())
        {
            return std::make_shared<LongitudinalDistanceAction>(longitudinal_action->GetLongitudinalDistanceAction(), environment, actors);
        }
    }
    else if (auto lateral_action = private_action->GetLateralAction())
    {
        if (auto lane_change_action = lateral_action->GetLaneChangeAction())
        {
            return std::make_shared<LaneChangeAction>(lane_change_action, environment, actors);
        }
    }
    else if (private_action->GetVisibilityAction())
    {
        return std::make_shared<VisibilityAction>(private_action->GetVisibilityAction(), environment, actors);
    }
    else if (private_action->GetRoutingAction())
    {
        auto routing_action = private_action->GetRoutingAction();
        if (routing_action->GetFollowTrajectoryAction())
        {
            return std::make_shared<FollowTrajectoryAction>(routing_action->GetFollowTrajectoryAction(), environment, actors);
        }
        else if (routing_action->GetAssignRouteAction())
        {
            return std::make_shared<AssignRouteAction>(routing_action->GetAssignRouteAction(), environment, actors);
        }
    }
    
    return nullptr;
}

std::shared_ptr<Action> StateMachineBuilder::CreateGlobalAction(std::shared_ptr<osc::IGlobalAction> global_action,
                                                                std::shared_ptr<mantle_api::IEnvironment> environment)
{
    if (auto infrastructure_action = global_action->GetInfrastructureAction())
    {
        auto traffic_signal_action = infrastructure_action->GetTrafficSignalAction();
        if (auto traffic_signal_state_action = traffic_signal_action->GetTrafficSignalStateAction())
        {
            return std::make_shared<TrafficSignalStateAction>(traffic_signal_state_action, environment);
        }
    }

    return nullptr;
}

std::shared_ptr<Action> StateMachineBuilder::CreateUserDefinedAction(std::shared_ptr<osc::IUserDefinedAction> user_defined_action,
                                                                     std::shared_ptr<mantle_api::IEnvironment> environment,
                                                                     std::vector<std::string> actors)
{
    if (user_defined_action->GetCustomCommandAction())
    {
        return std::make_shared<CustomCommandAction>(user_defined_action->GetCustomCommandAction(), environment, actors);
    }

    return nullptr;
}

void StateMachineBuilder::parseAction(/*const*/ osc::IEnvironmentAction& environment_action)
{
    std::ignore = environment_action;
}

void StateMachineBuilder::parseAction(/*const*/ osc::IAddEntityAction& add_entity_action)
{
    std::ignore = add_entity_action;
}

void StateMachineBuilder::parseAction(/*const*/ osc::IDeleteEntityAction& delete_entity_action)
{
    std::ignore = delete_entity_action;
}

void StateMachineBuilder::parseAction(/*const*/ osc::IParameterSetAction& parameter_set_action)
{
    std::ignore = parameter_set_action;
}

void StateMachineBuilder::parseAction(/*const*/ osc::IParameterModifyAction& parameter_modify_action)
{
    std::ignore = parameter_modify_action;
}

void StateMachineBuilder::parseAction(/*const*/ osc::ITrafficSignalControllerAction& trafficSignalController_action)
{
    std::ignore = trafficSignalController_action;
}

void StateMachineBuilder::parseAction(/*const*/ osc::ITrafficSignalStateAction& trafficSignalState_action)
{
    std::ignore = trafficSignalState_action;
}

void StateMachineBuilder::parseAction(/*const*/ osc::ITrafficSourceAction& traffic_source_action)
{
    std::ignore = traffic_source_action;
}

void StateMachineBuilder::parseAction(/*const*/ osc::ITrafficSinkAction& traffic_sink_action)
{
    std::ignore = traffic_sink_action;
}

void StateMachineBuilder::parseAction(/*const*/ osc::ITrafficSwarmAction& traffic_swarm_action)
{
    std::ignore = traffic_swarm_action;
}

}  // namespace OPENSCENARIO

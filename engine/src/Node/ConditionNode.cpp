/*******************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Node/ConditionNode.h"

#include "Utils/ConditionEdgeEvaluator.h"
#include "agnostic_behavior_tree/behavior_node.h"
#include "units.h"

namespace yase
{
ConditionNode::ConditionNode(std::string name,
                             double delay,
                             OPENSCENARIO::ConditionEdge condition_edge,
                             yase::BehaviorNode::Ptr child)
    : DecoratorNode(std::move(name)),
      delay_{units::make_unit<units::time::second_t>(delay)},
      edge_evaluator_{condition_edge}
{
  setChild(child);
}

void ConditionNode::lookupAndRegisterData(yase::Blackboard& blackboard)
{
  environment_ = blackboard.get<std::shared_ptr<mantle_api::IEnvironment>>("Environment");
}

NodeStatus ConditionNode::tick()
{
  using namespace units::literals;

  const auto current_time = environment_->GetSimulationTime();

  if (!start_time_)
  {
    start_time_ = current_time;
  }

  const auto child_status = child().executeTick();
  if (child_status == yase::NodeStatus::kFailure)
  {
    return yase::NodeStatus::kFailure;
  }

  const auto status = (child_status == yase::NodeStatus::kSuccess);
  result_buffer_[current_time] = edge_evaluator_.is_satisfied(status);

  const auto previous_result_time = current_time - delay_;

  // Earlier than first recorded result
  if (previous_result_time < start_time_.value_or(0_s))
  {
    return NodeStatus::kRunning;
  }

  UpdateBuffer(previous_result_time);
  return result_buffer_.begin()->second ? yase::NodeStatus::kSuccess : yase::NodeStatus::kRunning;
};

void ConditionNode::UpdateBuffer(mantle_api::Time previous_result_time)
{
  auto it = result_buffer_.find(previous_result_time);
  // If there is no result for the exact previous time, then take the result of one step before
  if (it == result_buffer_.end())
  {
    it = result_buffer_.lower_bound(previous_result_time);
    it--;
  }
  // Erase all results from before to avoid memory leak
  result_buffer_.erase(result_buffer_.begin(), it);
}

}  // namespace yase
# OpenSCENARIO 1 Engine
This is an approach for an OpenSCENARIO 1 engine, which uses a generic interface between an environment simulator and the engine (MantleAPI). The advantage is that it can be used with different simulators, which implement the generic interface.

Remark: The engine in this folder 'existing_open_scenario_engine' is in a working state, but deprecated. In the future only the engine in the folder 'engine' shall be used. This folder will then be deleted.

## License
EPL 2.0

## Third Party Libraries in use

### scenario_api (MantleAPI)
- License: Eclipse Public License 2.0
- Repository: https://gitlab.eclipse.org/eclipse/simopenpass/scenario_api
- Commit: d07513693d12d04700554afc735f2ffcb076c270

### OpenSCENARIO API (Parser)
- License: Apache-2.0
- Repository: https://github.com/RA-Consulting-GmbH/openscenario.api.test
- Version: 1.3.0
- Commit: 24dbd3fdc8ada5746cb6698d3172cad2753f65ac

/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Utils/ControllerCreator.h"

#include <openScenarioLib/generated/v1_1/catalog/CatalogHelperV1_1.h>

#include <iostream>

#include "Utils/Constants.h"

namespace OPENSCENARIO
{
ControllerCreator::ControllerCreator(std::shared_ptr<mantle_api::IEnvironment> environment)
    : environment_(environment)
{
}

void ControllerCreator::CreateControllers(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IScenarioObject> scenario_object)
{
  if (IsControllableObject(scenario_object))
  {
    if (scenario_object->GetObjectController() ||
        (IsDrivingFunctionControlledEntity(scenario_object->GetName()) && is_host_controlled_externally_))
    {
      std::cout << "ControllerCreator: Setting up external controller for entity " << scenario_object->GetName()
                << std::endl;
      SetupEntityWithExternalConfigs(scenario_object);
    }
    else
    {
      std::cout << "ControllerCreator: Setting up internal controller for entity " << scenario_object->GetName()
                << std::endl;
      SetupEntityWithInternalConfigs(scenario_object);
    }
  }
  else
  {
    std::cout << "ControllerCreator: No controller created for non-vehicle and non-pedestrian type scenario object "
              << scenario_object->GetName() << std::endl;
  }
}

bool ControllerCreator::IsControllableObject(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IScenarioObject> scenario_object)
{
  auto entity_object = scenario_object->GetEntityObject();

  if (!entity_object)
  {
    throw std::runtime_error(std::string("entityObject missing in ScenarioObject " + scenario_object->GetName()));
  }

  if (entity_object->GetVehicle() != nullptr || entity_object->GetPedestrian() != nullptr)
  {
    return true;
  }
  else if (entity_object->GetCatalogReference() != nullptr)
  {
    return IsCatalogReferenceControllableObject(entity_object->GetCatalogReference());
  }

  return false;
}

bool ControllerCreator::IsCatalogReferenceControllableObject(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ICatalogReference> catalog_reference)
{
  auto catalog_element = catalog_reference->GetRef();
  if (catalog_element == nullptr)
  {
    throw std::runtime_error(std::string("CatalogReference " + catalog_reference->GetEntryName() +
                                         " for entity object cannot be resolved."));
  }

  return NET_ASAM_OPENSCENARIO::v1_1::CatalogHelper::IsVehicle(catalog_element) ||
         NET_ASAM_OPENSCENARIO::v1_1::CatalogHelper::IsPedestrian(catalog_element);
}

void ControllerCreator::SetupEntityWithInternalConfigs(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IScenarioObject> scenario_object)
{
  // Create internal config with default control strategy (keep lane, keep speed)
  auto internal_config = std::make_unique<mantle_api::InternalControllerConfig>();
  internal_config->control_strategies.push_back(std::make_unique<mantle_api::KeepVelocityControlStrategy>());
  internal_config->control_strategies.push_back(std::make_unique<mantle_api::KeepLaneOffsetControlStrategy>());

  auto& controller = environment_->GetControllerRepository().Create(std::move(internal_config));

  AddEntityToController(scenario_object, controller.GetUniqueId());
}

void ControllerCreator::SetupEntityWithExternalConfigs(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IScenarioObject> scenario_object)
{
  std::unique_ptr<mantle_api::ExternalControllerConfig> external_config;

  if (scenario_object->GetObjectController() == nullptr)  // external host
  {
    external_config = CreateExternalControllerConfig(nullptr);
  }
  else if (scenario_object->GetObjectController()->GetController() != nullptr)
  {
    external_config = CreateExternalControllerConfig(scenario_object->GetObjectController()->GetController());
  }
  else if (auto catalog_reference = scenario_object->GetObjectController()->GetCatalogReference())
  {
    auto ref = catalog_reference->GetRef();
    if (NET_ASAM_OPENSCENARIO::v1_1::CatalogHelper::IsController(ref))
    {
      external_config =
          CreateExternalControllerConfig(NET_ASAM_OPENSCENARIO::v1_1::CatalogHelper::AsController(ref));
    }
    else
    {
      throw std::runtime_error(std::string("CatalogReference " + catalog_reference->GetEntryName() +
                                           " for object controller cannot be resolved."));
    }
  }

  auto& controller = environment_->GetControllerRepository().Create(std::move(external_config));
  AddEntityToController(scenario_object, controller.GetUniqueId());
}

void ControllerCreator::AddEntityToController(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IScenarioObject> scenario_object,
    const mantle_api::UniqueId& id)
{
  auto entity = environment_->GetEntityRepository().Get(scenario_object->GetName());

  if (entity.has_value())
  {
    environment_->AddEntityToController(entity.value(), id);
  }
}

void ControllerCreator::SetExternalControllerOverride()
{
  is_host_controlled_externally_ = true;
}

std::unique_ptr<mantle_api::ExternalControllerConfig> ControllerCreator::CreateExternalControllerConfig(
    const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IController> controller)
{
  // Create generic external config which can be resolved by the environment
  auto external_config = std::make_unique<mantle_api::ExternalControllerConfig>();

  if (controller != nullptr)
  {
    external_config->name = controller->GetName();

    for (const auto& property : controller->GetProperties()->GetProperties())
    {
      external_config->parameters.emplace(property->GetName(), property->GetValue());
    }
  }

  return external_config;
}

}  // namespace OPENSCENARIO

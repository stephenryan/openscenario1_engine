/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>
#include <agnostic_behavior_tree/action_node.h>

#include <cassert>

#include "Storyboard/ByValueCondition/TrafficSignalControllerCondition_impl.h"

class TrafficSignalControllerCondition : public yase::ActionNode
{
public:
  TrafficSignalControllerCondition(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITrafficSignalControllerCondition> trafficSignalControllerCondition)
      : yase::ActionNode{"TrafficSignalControllerCondition"},
        trafficSignalControllerCondition_{trafficSignalControllerCondition}
  {
  }

  void onInit() override{};

private:
  yase::NodeStatus tick() override
  {
    assert(impl_);
    return impl_->IsSatisfied() ? yase::NodeStatus::kSuccess : yase::NodeStatus::kRunning;
  };

  void lookupAndRegisterData(yase::Blackboard& blackboard) final
  {
    auto environment = blackboard.get<std::shared_ptr<mantle_api::IEnvironment>>("Environment");

    impl_ = std::make_unique<OpenScenarioEngine::v1_1::TrafficSignalControllerCondition>(
        OpenScenarioEngine::v1_1::TrafficSignalControllerCondition::Values{
            trafficSignalControllerCondition_->GetPhase(),
            OPENSCENARIO::ConvertScenarioTrafficSignalController(trafficSignalControllerCondition_->GetTrafficSignalControllerRef()),
            OPENSCENARIO::ConvertScenarioPhase(trafficSignalControllerCondition_->GetPhaseRef())},
        OpenScenarioEngine::v1_1::TrafficSignalControllerCondition::Interfaces{
            environment});
  }

  std::unique_ptr<OpenScenarioEngine::v1_1::TrafficSignalControllerCondition> impl_{nullptr};
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITrafficSignalControllerCondition> trafficSignalControllerCondition_;
};
/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Test/test_utils.h>
#include <gtest/gtest.h>
#include <openScenarioLib/generated/v1_1/api/ApiClassInterfacesV1_1.h>
#include <openScenarioLib/generated/v1_1/impl/ApiClassImplV1_1.h>

#include <vector>

#include "Conversion/OscToMantle/ConvertScenarioTrajectoryRef.h"

using namespace units::literals;

class ConvertScenarioTrajectoryRefTest : public ::testing::Test
{
protected:
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::PositionImpl> SetWorldPosition(double x, double y, double z, double h, double p, double r)
  {
    auto world_pos = std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::WorldPositionImpl>();
    world_pos->SetX(x);
    world_pos->SetY(y);
    world_pos->SetZ(z);
    world_pos->SetH(h);
    world_pos->SetP(p);
    world_pos->SetR(r);
    auto pos = std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::PositionImpl>();
    pos->SetWorldPosition(world_pos);
    return pos;
  }

  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::VertexImpl> SetVertex(double x, double y, double z, double h, double p, double r)
  {
    auto worldposition = SetWorldPosition(x, y, z, h, p, r);

    auto vertex = std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::VertexImpl>();
    vertex->SetPosition(worldposition);
    vertex->SetTime(5.0);
    return vertex;
  }

  mantle_api::Vec3<units::length::meter_t> position1{1.0_m, 2.0_m, 0.0_m};
  mantle_api::Orientation3<units::angle::radian_t> orientation1{0.1_rad, 0.0_rad, 0.0_rad};
  mantle_api::Vec3<units::length::meter_t> position2{1.1_m, 2.1_m, 0.0_m};
  mantle_api::Orientation3<units::angle::radian_t> orientation2{0.2_rad, 0.0_rad, 0.0_rad};
  mantle_api::Vec3<units::length::meter_t> position3{1.2_m, 2.2_m, 0.0_m};
  mantle_api::Orientation3<units::angle::radian_t> orientation3{0.3_rad, 0.0_rad, 0.0_rad};

  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::PolylineImpl> polyLine{std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::PolylineImpl>()};
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ShapeImpl> trajectoryShape{std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::ShapeImpl>()};
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::TrajectoryImpl> trajectory{std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::TrajectoryImpl>()};
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::TrajectoryRefImpl> trajectoryRef{std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::TrajectoryRefImpl>()};

  std::shared_ptr<mantle_api::MockEnvironment> mockEnvironment{std::make_shared<mantle_api::MockEnvironment>()};
};

TEST_F(ConvertScenarioTrajectoryRefTest, GivenPolyLine_ReturnMantleAPITrajectoryType)
{
  std::vector<std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IVertexWriter>> vertices{};
  vertices.push_back(SetVertex(position1.x.value(), position1.y.value(), position1.z.value(), orientation1.yaw.value(), orientation1.pitch.value(), orientation1.roll.value()));
  vertices.push_back(SetVertex(position2.x.value(), position2.y.value(), position2.z.value(), orientation2.yaw.value(), orientation2.pitch.value(), orientation2.roll.value()));
  vertices.push_back(SetVertex(position3.x.value(), position3.y.value(), position3.z.value(), orientation3.yaw.value(), orientation3.pitch.value(), orientation3.roll.value()));

  polyLine->SetVertices(vertices);
  trajectoryShape->SetPolyline(polyLine);
  trajectory->SetShape(trajectoryShape);
  trajectory->SetName("Trajectory1");
  trajectoryRef->SetTrajectory(trajectory);

  auto convertedTrajectoryRef = OPENSCENARIO::ConvertScenarioTrajectoryRef(mockEnvironment, trajectoryRef, nullptr);

  mantle_api::Pose expected_pose1{position1, orientation1};
  mantle_api::Pose expected_pose2{position2, orientation2};
  mantle_api::Pose expected_pose3{position3, orientation3};

  auto& polyline = std::get<mantle_api::PolyLine>(convertedTrajectoryRef->type);
  EXPECT_THAT(polyline, testing::SizeIs(3));
  EXPECT_EQ(polyline.at(0).time, 5.0_s);
  EXPECT_EQ(polyline.at(0).pose, expected_pose1);
  EXPECT_EQ(polyline.at(1).time, 5.0_s);
  EXPECT_EQ(polyline.at(1).pose, expected_pose2);
  EXPECT_EQ(polyline.at(2).time, 5.0_s);
  EXPECT_EQ(polyline.at(2).pose, expected_pose3);
}

TEST_F(ConvertScenarioTrajectoryRefTest, GivenNoPolyLine_ReturnNullPtr)
{
  trajectory->SetShape(trajectoryShape);
  trajectory->SetName("Trajectory1");
  trajectoryRef->SetTrajectory(trajectory);

  // TrajectoryRef ConvertScenarioTrajectoryRef(std::shared_ptr<mantle_api::IEnvironment> environment,
  //                                            std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITrajectoryRef> trajectoryRef,
  //                                            std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ICatalogReference> catalogReference)

  auto convertedTrajectoryRef = OPENSCENARIO::ConvertScenarioTrajectoryRef(mockEnvironment, trajectoryRef, nullptr);

  EXPECT_EQ(std::nullopt, convertedTrajectoryRef);
}
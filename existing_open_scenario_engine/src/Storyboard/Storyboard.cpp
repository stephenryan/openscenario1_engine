/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Storyboard.h"

#include "StateMachine/StateMachineBuilder.h"

#include <MantleAPI/Common/floating_point_helper.h>

namespace OPENSCENARIO
{
Storyboard::Storyboard(NET_ASAM_OPENSCENARIO::v1_1::IStoryboard& storyboard,
                       std::shared_ptr<mantle_api::IEnvironment> environment)
    : data_(&storyboard), environment_(environment)
{
    CreateInitActions();
    CreateStories();
    CreateStopTriggerIfExists();
}

void Storyboard::Init()
{
    is_finished_ = false;

    // start init actions
    for (auto init_action : init_actions)
    {
        init_action->Start();
    }

    // Alternative state machine approach
    state_machine_.Step();
}

void Storyboard::Step()
{
    // Check overall stop trigger
    if (!is_finished_ && stop_trigger_ != nullptr)
    {
        is_finished_ = stop_trigger_->IsSatisfied();
    }

    // step init actions (execute actions by calling the environment api and update internal state)
    for (auto init_action : init_actions)
    {
        init_action->Step();
    }

    // step stories (execute actions by calling the environment api and update internal state)
    // TODO: Replace with stories, once they are instantiated
    for (auto event : events)
    {
        event->Step();
    }

    // Check for each entity, if an action/control strategy is active in lat and lon domain and if not, then
    // assign default control strategy to this domain

    // Alternative state machine approach
    state_machine_.Step();
}

bool Storyboard::IsFinished()
{
    // this will be set from the stop trigger in the end
    return is_finished_;

    // Alternative state machine approach
    // return dynamic_cast<const CompleteState*>(state_machine_.GetActiveState()) != nullptr;
}

mantle_api::Time Storyboard::GetDuration() const
{
    auto condition_groups = data_->GetStopTrigger()->GetConditionGroups();
    std::vector<double> durations{};
    for (const auto& condition_group : condition_groups)
    {
        auto conditions = condition_group->GetConditions();
        std::for_each(conditions.begin(), conditions.end(), [&durations](const auto& condition) {
            if (const auto& by_val_condition = condition->GetByValueCondition())
            {
                if (const auto& sim_time_condition = by_val_condition->GetSimulationTimeCondition())
                {
                    durations.push_back(sim_time_condition->GetValue());
                }
            }
        });
    }
    std::sort(durations.begin(), durations.end(), std::greater<double>());
    return durations.empty() ? mantle_api::Time{std::numeric_limits<double>::max()}
                             : mantle_api::Time{units::time::second_t{durations.at(0)}};
}

void Storyboard::CreateInitActions()
{
    if (data_->GetInit() == nullptr || data_->GetInit()->GetActions() == nullptr)
    {
        return;
    }

    auto private_init_actions = data_->GetInit()->GetActions()->GetPrivates();
    for (auto private_init_action : private_init_actions)
    {
        std::vector<std::string> actors;
        actors.push_back(private_init_action->GetEntityRef()->GetNameRef());

        for (auto parsed_private_action : private_init_action->GetPrivateActions())
        {
            auto private_action = StateMachineBuilder::CreatePrivateAction(parsed_private_action, environment_, actors);
            if (private_action != nullptr)
            {
                init_actions.push_back(private_action);
            }
        }
    }

    auto global_init_actions = data_->GetInit()->GetActions()->GetGlobalActions();
    for(auto global_init_action : global_init_actions)
    {
        auto global_action = StateMachineBuilder::CreateGlobalAction(global_init_action, environment_);
        if (global_action != nullptr)
        {
            init_actions.push_back(global_action);
        }
    }

    auto user_defined_init_actions = data_->GetInit()->GetActions()->GetUserDefinedActions();
    for (auto user_defined_init_action : user_defined_init_actions)
    {
        std::vector<std::string> actors{};
        auto user_defined_action = StateMachineBuilder::CreateUserDefinedAction(user_defined_init_action, environment_, actors);
        if (user_defined_action != nullptr)
        {
            init_actions.push_back(user_defined_action);
        }
    }
}

void Storyboard::CreateStories()
{
    // TODO: Also instantiate stories, acts, maneuver groups and maneuvers and not only events
    for (auto story : data_->GetStories())
    {
        for (auto act : story->GetActs())
        {
            for (auto maneuver_group : act->GetManeuverGroups())
            {
                std::vector<std::string> actors;
                if (maneuver_group->GetActors() != nullptr)
                {
                    for (auto entity_ref : maneuver_group->GetActors()->GetEntityRefs())
                    {
                        actors.push_back(entity_ref->GetEntityRef()->GetNameRef());
                    }
                }

                for (auto maneuver : maneuver_group->GetManeuvers())
                {
                    for (auto event : maneuver->GetEvents())
                    {
                        events.push_back(std::make_shared<Event>(event, environment_, actors));
                    }
                }
            }
        }
    }
}

void Storyboard::CreateStopTriggerIfExists()
{
    if (data_->GetStopTrigger() == nullptr)
    {
        return;
    }
    stop_trigger_ = std::make_unique<Trigger>(data_->GetStopTrigger(), environment_);
}

}  // namespace OPENSCENARIO

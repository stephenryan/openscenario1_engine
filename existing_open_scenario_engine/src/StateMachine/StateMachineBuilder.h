/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG),
 *                     in-tech GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "Storyboard/Actions/Action.h"

#include <openScenarioLib/generated/v1_1/api/ApiClassInterfacesV1_1.h>

namespace OPENSCENARIO
{
namespace osc = NET_ASAM_OPENSCENARIO::v1_1;

class StateMachineBuilder
{
  public:
    void parseStoryboard(/*const*/ osc::IStoryboard& storyboard);

    static std::shared_ptr<Action> CreatePrivateAction(std::shared_ptr<osc::IPrivateAction> private_action,
                                                       std::shared_ptr<mantle_api::IEnvironment> environment,
                                                       std::vector<std::string> actors);

    static std::shared_ptr<Action> CreateGlobalAction(std::shared_ptr<osc::IGlobalAction> global_action,
                                                       std::shared_ptr<mantle_api::IEnvironment> environment);

    static std::shared_ptr<Action> CreateUserDefinedAction(std::shared_ptr<osc::IUserDefinedAction> user_defined_action,
                                                           std::shared_ptr<mantle_api::IEnvironment> environment,
                                                           std::vector<std::string> actors);

  private:
    void parseGlobalAction(/*const*/ osc::IGlobalAction& global_action);

    /* global actions */
    void parseAction(/*const*/ osc::IEnvironmentAction& environment_action);

    void parseAction(/*const*/ osc::IAddEntityAction& add_entity_action);

    void parseAction(/*const*/ osc::IDeleteEntityAction& delete_entity_action);

    void parseAction(/*const*/ osc::IParameterSetAction& parameter_set_action);

    void parseAction(/*const*/ osc::IParameterModifyAction& parameter_modify_action);

    void parseAction(/*const*/ osc::ITrafficSignalControllerAction& trafficSignalController_action);

    void parseAction(/*const*/ osc::ITrafficSignalStateAction& trafficSignalState_action);

    void parseAction(/*const*/ osc::ITrafficSourceAction& traffic_source_action);

    void parseAction(/*const*/ osc::ITrafficSinkAction& traffic_sink_action);

    void parseAction(/*const*/ osc::ITrafficSwarmAction& traffic_swarm_action);

    void parseUserDefinedAction(/*const*/ osc::IUserDefinedAction& user_defined_action);

    void parsePrivateAction(/*const*/ osc::IPrivateAction& private_action);
};

}  // namespace OPENSCENARIO

/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Event.h"

#include "StateMachine/StateMachineBuilder.h"

namespace OPENSCENARIO
{

OPENSCENARIO::Event::Event(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IEvent> event_data,
                           std::shared_ptr<mantle_api::IEnvironment> environment,
                           std::vector<std::string> actors)
    : start_trigger_(event_data->GetStartTrigger(), environment), data_(event_data)
{
    for (auto action : event_data->GetActions())
    {
        if (action->GetPrivateAction() != nullptr)
        {
            auto private_action =
                StateMachineBuilder::CreatePrivateAction(action->GetPrivateAction(), environment, actors);
            if (private_action != nullptr)
            {
                actions.push_back(private_action);
            }
        }
        else if (action->GetGlobalAction() != nullptr)
        {
            auto global_action = StateMachineBuilder::CreateGlobalAction(action->GetGlobalAction(), environment);
            if (global_action != nullptr)
            {
                actions.push_back(global_action);
            }
        }
        else if (action->GetUserDefinedAction() != nullptr)
        {
            auto user_defined_action =
                StateMachineBuilder::CreateUserDefinedAction(action->GetUserDefinedAction(), environment, actors);
            if (user_defined_action != nullptr)
            {
                actions.push_back(user_defined_action);
            }
        }
    }
}

void Event::Step()
{
    if (isStandby())
    {
        if (start_trigger_.IsSatisfied())
        {
            Start();
            for (auto action : actions)
            {
                action->Start();
            }
        }
    }

    if (isRunning())
    {
        for (auto action : actions)
        {
            action->Step();
        }
    }
}

void Event::ExecuteStartTransition()
{
    std::cout << "OpenScenarioEngine: Starting Event '" << data_->GetName() << "'" << std::endl;
}

}  // namespace OPENSCENARIO

/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2021 Max Paul Bauer - Robert Bosch GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include "Node/TriggerableCompositeNode.h"

namespace yase
{
class ActNode : public TriggerableCompositeNode
{
public:
  ActNode(std::string name,
          yase::BehaviorNode::Ptr maneuver_groups,
          yase::BehaviorNode::Ptr start_trigger = nullptr,
          yase::BehaviorNode::Ptr stop_trigger = nullptr)
      : TriggerableCompositeNode{std::move(name)}
  {
    set(maneuver_groups, StopTriggerPtr{stop_trigger}, StartTriggerPtr{start_trigger});
  }
};

}  // namespace yase
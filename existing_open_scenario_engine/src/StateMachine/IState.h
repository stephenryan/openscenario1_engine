/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "StateMachine/StateMachine.h"

namespace OPENSCENARIO
{
class IStateMachine;
}

namespace OPENSCENARIO
{
class IState
{
  public:
    virtual ~IState() = default;

    virtual void Step() = 0;

    virtual void SetContext(IStateMachine& context) = 0;

    virtual void SetTransitionToState(IState& next_state) = 0;
};

}  // namespace OPENSCENARIO

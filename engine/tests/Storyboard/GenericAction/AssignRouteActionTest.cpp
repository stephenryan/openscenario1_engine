/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Test/test_utils.h>
#include <gmock/gmock-matchers.h>
#include <gtest/gtest.h>

#include "Conversion/OscToMantle/ConvertScenarioRoute.h"
#include "Storyboard/GenericAction/AssignRouteAction_impl.h"
#include "gmock/gmock.h"

using namespace mantle_api;
using testing::_;
using testing::NotNull;
using testing::Return;
using testing::SizeIs;
using namespace units::literals;

TEST(AssignRouteAction, IsStepped_ReturnsTrue)
{
  OpenScenarioEngine::v1_1::AssignRouteAction assignRouteAction({std::vector<std::string>{"Vehicle1"},
                                                                 OPENSCENARIO::Route{}},
                                                                {std::make_shared<MockEnvironment>()});
  ASSERT_TRUE(assignRouteAction.Step());
}

TEST(DISABLED_AssignRouteAction, GivenClosedRoute_NotImplementedYet)
{
  OPENSCENARIO::Route route{};
  route.closed = true;
  OpenScenarioEngine::v1_1::AssignRouteAction assignRouteAction({std::vector<std::string>{"Vehicle1"},
                                                                 route},
                                                                {std::make_shared<MockEnvironment>()});
}

TEST(AssignRouteAction, IsStepped_PropagatesWaypoints)
{
  Vec3<units::length::meter_t> expected_position{1.2_m, 3.4_m, 5.6_m};

  OPENSCENARIO::Route route{};
  route.waypoints.push_back(RouteWaypoint{expected_position, RouteStrategy::kUndefined});

  mantle_api::RouteDefinition route_definition{};

  auto mockEnvironment = std::make_shared<MockEnvironment>();
  EXPECT_CALL(*mockEnvironment, AssignRoute(_, _))
      .WillOnce(testing::SaveArg<1>(&route_definition));

  OpenScenarioEngine::v1_1::AssignRouteAction assignRouteAction({{"Vehicle1"}, route},
                                                                {mockEnvironment});

  assignRouteAction.Step();

  ASSERT_THAT(route_definition.waypoints, SizeIs(1));
  EXPECT_THAT(route_definition.waypoints[0].waypoint, expected_position);
}